﻿// Copyright (c) 2012 Fc <fcx.code@gmail.com>
//            All Rights Reserved
//
// This software is provided 'as-is', without any express or implied 
// warranty. In no event will the author be held liable for any damages 
// arising from the use of this software.
//
namespace EGSoft.Fc.SubspaceModel.Modules

open System
open System.Collections
open System.Collections.Generic
open System.Collections.Concurrent
open LanguagePrimitives

open EGSoft.Fc.EngineModel
open EGSoft.Fc.SubspaceModel
open EGSoft.Fc.SubspaceModel.Utilities
open EGSoft.Fc.SubspaceModel.Net
open EGSoft.Fc.SubspaceModel.Net.Utilities
open EGSoft.Fc.SubspaceModel.Net.Packets
open EGSoft.Fc.SubspaceModel.Net.Packets.Core
open EGSoft.Fc.SubspaceModel.Net.Packets.C2S
open EGSoft.Fc.SubspaceModel.Net.Packets.S2C
open EGSoft.Fc.SubspaceModel.Net.Security


type ArenaEnteringHandle = delegate of sender:obj * userId:uint16 -> unit
type ArenaEnteredHandle = delegate of sender:obj * userId:uint16 -> unit
type ArenaInfoListHandle = delegate of sender:obj * arenaInfoList:ArenaInfoList -> unit

type PlayerEnterHandle = delegate of sender:obj * playerInfo:PlayerInfo -> unit
type PlayerLeaveHandle = delegate of sender:obj * playerInfo:PlayerInfo -> unit
type PlayerFreqChangeHandle = delegate of sender:obj * playerInfo:PlayerInfo -> unit
type PlayerScoreUpdateHandle = delegate of sender:obj * playerInfo:PlayerInfo -> unit
type PlayerShipChangeHandle = delegate of sender:obj * playerInfo:PlayerInfo -> unit
type PlayerDiedhandle = delegate of sender:obj * playerInfo:PlayerInfo -> unit

[<AllowNullLiteral>]
type IArena =
    inherit IFcEngineModule
    [<CLIEvent>]
    abstract ArenaEntering : IEvent<ArenaEnteringHandle,_>
    [<CLIEvent>]
    abstract ArenaEntered : IEvent<ArenaEnteredHandle,_>
    [<CLIEvent>]
    abstract ArenaInfoList : IEvent<ArenaInfoListHandle,ArenaInfoList>
    [<CLIEvent>]
    abstract PlayerEnter : IEvent<PlayerEnterHandle,PlayerInfo>
    [<CLIEvent>]
    abstract PlayerLeave : IEvent<PlayerLeaveHandle, PlayerInfo>
    [<CLIEvent>]
    abstract PlayerFreqChange : IEvent<PlayerFreqChangeHandle, PlayerInfo>
    [<CLIEvent>]
    abstract PlayerShipChange : IEvent<PlayerShipChangeHandle, PlayerInfo>
    [<CLIEvent>]
    abstract PlayerScoreUpdate : IEvent<PlayerScoreUpdateHandle, PlayerInfo>

    abstract Name : String
        with get, set
    abstract UserId : unit -> uint16
        with get
    abstract UserTargetingPlayerId : unit -> uint16
        with get
    abstract TargetPlayer : uint16 -> bool
    abstract TargetPlayer : String -> bool
    abstract PlayerInfoMap : unit -> PlayerInfoMap
        with get
    abstract TeamInfoMap : unit -> TeamInfoMap
        with get
    abstract Go : shipType:ShipType * arenaName:string -> unit


[<FcEngineModuleInfo(
    ModuleName = "Arena",
    Author = "Fc",
    Version = "v1.0")>]
type Arena() =
    let mutable name = String.Empty
    let mutable userId = 0us
    let mutable userTargetingPlayerId = 0us

    let arenaEnteringEvent = new Event<ArenaEnteringHandle,_>()
    let arenaEnteredEvent = new Event<ArenaEnteredHandle,_>()
    
    let arenaInfoListEvent = new Event<ArenaInfoListHandle, ArenaInfoList>()

    let mutable playerInfoMap : PlayerInfoMap = new PlayerInfoMap()
    let mutable teamInfoMap : TeamInfoMap = new TeamInfoMap()

    let playerEnterEvent = new Event<PlayerEnterHandle,PlayerInfo>()
    let playerLeaveEvent = new Event<PlayerLeaveHandle, PlayerInfo>()
    let playerFreqChangeEvent = new Event<PlayerFreqChangeHandle,PlayerInfo>()
    let playerScoreUpdateEvent = new Event<PlayerScoreUpdateHandle,PlayerInfo>()
    let playerShipChangeEvent = new Event<PlayerShipChangeHandle,PlayerInfo>()

    let mutable _linker:IFcEngineLinker = null
    let mutable _zone:IZone = null

    interface IArena with
        member this.OnLoad(linker:IFcEngineLinker) =
            _linker <- linker
            _zone <- linker.AttachModule<IZone>()
            _zone.RecievedPacketAddHandler((byte)(EnumToValue PacketS2CType.ArenaEntering), new ReceivedPacketHandle(this.ArenaEntering))
            _zone.RecievedPacketAddHandler((byte)(EnumToValue PacketS2CType.ArenaEntered), new ReceivedPacketHandle(this.ArenaEnteredHandler))
            _zone.RecievedPacketAddHandler((byte)(EnumToValue PacketS2CType.KOTHSetTimer), new ReceivedPacketHandle(this.KOTHSetTimerHandler))
            _zone.RecievedPacketAddHandler((byte)(EnumToValue PacketS2CType.KOTHReset), new ReceivedPacketHandle(this.KOTHResetHandler))
            _zone.RecievedPacketAddHandler((byte)(EnumToValue PacketS2CType.ArenaList), new ReceivedPacketHandle(this.ArenaListHandler))
            _zone.RecievedPacketAddHandler((byte)(EnumToValue PacketS2CType.PlayersEnter), new ReceivedPacketHandle(this.PlayersEnterHandler))
            _zone.RecievedPacketAddHandler((byte)(EnumToValue PacketS2CType.PlayerLeave), new ReceivedPacketHandle(this.PlayerLeaveHandler))
            _zone.RecievedPacketAddHandler((byte)(EnumToValue PacketS2CType.PlayerFreqChange), new ReceivedPacketHandle(this.PlayerFreqChangeHandler))
            _zone.RecievedPacketAddHandler((byte)(EnumToValue PacketS2CType.PlayerShipChange), new ReceivedPacketHandle(this.PlayerShipChangeHandler))
            _zone.RecievedPacketAddHandler((byte)(EnumToValue PacketS2CType.PlayerScoreUpdate), new ReceivedPacketHandle(this.PlayerScoreUpdateHandler))
            _zone.RecievedPacketAddHandler((byte)(EnumToValue PacketS2CType.PlayerDied), new ReceivedPacketHandle(this.PlayerDiedHandler))
            //botEngine.RecievedPacket(PacketS2CType.FileTransfer).AddHandler(new Handler<PacketReader>(fun sender packet -> Console.WriteLine("Got File")))
            true
        member this.OnUnload(linker:IFcEngineLinker) =
            true

        [<CLIEvent>]
        member this.ArenaEntering = arenaEnteringEvent.Publish
        [<CLIEvent>]
        member this.ArenaEntered = arenaEnteredEvent.Publish


        [<CLIEvent>]
        member this.ArenaInfoList = arenaInfoListEvent.Publish
        [<CLIEvent>]
        member this.PlayerEnter = playerEnterEvent.Publish
        [<CLIEvent>]
        member this.PlayerLeave = playerLeaveEvent.Publish
        [<CLIEvent>]
        member this.PlayerFreqChange = playerFreqChangeEvent.Publish
        [<CLIEvent>]
        member this.PlayerScoreUpdate = playerScoreUpdateEvent.Publish
        [<CLIEvent>]
        member this.PlayerShipChange = playerShipChangeEvent.Publish

        member this.Name 
            with get() = name
            and set v = name <- v

        member this.UserId = userId
        member this.UserTargetingPlayerId = userTargetingPlayerId
        member this.PlayerInfoMap = playerInfoMap
        member this.TeamInfoMap = teamInfoMap

        member this.TargetPlayer(playerName:String) =
            let mutable pi = null
            if (playerInfoMap.TryGetPlayer(playerName, &pi)) then
                userTargetingPlayerId <- pi.PlayerId
                true
            else
                false

        member this.TargetPlayer(playerId:uint16) =
            let mutable pi = null
            if (playerInfoMap.TryGetPlayer(playerId, &pi)) then
                userTargetingPlayerId <- playerId
                true
            else
                false

        member this.Go (shipType :ShipType, arenaName : String) =
            //_zone.SendPacket(new ArenaLeave(), true)
            let arenaEnter = new ArenaEnter(
                                    shipType, 
                                    AudioType.None, 
                                    1280us, 
                                    1024us,
                                    match arenaName with
                                    | "" -> EnumToValue ArenaLoginType.UseRandomPublic
                                    | _ ->
                                            match System.UInt16.TryParse(arenaName) with
                                            | true, publicNum -> publicNum
                                            | _ -> EnumToValue ArenaLoginType.UseArenaName
                                    ,arenaName)
            _zone.SendPacket(arenaEnter, true)

    member this.ArenaEntering sender packet =
        playerInfoMap.Clear()
        teamInfoMap.Clear()
        let a = new ArenaEntering(packet)
        userId <- a.PlayerId
        userTargetingPlayerId <- userId
        arenaEnteringEvent.Trigger(this, userId)
        

    member this.ArenaEnteredHandler sender packet =
        _zone.SendPacket(new Position(0uy, 0u, 0us, 0us, 0s, 0s, 0us, 0us, 0uy, 0us), true)
        arenaEnteredEvent.Trigger(this, userId)

    member private this.ArenaListHandler sender packet =
        let arenaList = new ArenaList(packet)
        let arenaInfo = new ArenaInfoList(arenaList.ArenaInfoArray)
        arenaInfoListEvent.Trigger(this, arenaInfo)

    member private this.KOTHResetHandler sender packet =
        _zone.SendPacket(new KotHEndTimer(), true)

    member private this.KOTHSetTimerHandler sender packet = 
        ()

    member private this.PlayersEnterHandler sender packet =   
        let playersEnter = new PlayersEnter(packet)
        for playerInfo in playersEnter.PlayerInfoArray do
            playerInfoMap.Add(playerInfo)
            teamInfoMap.Add(playerInfo)
            playerEnterEvent.Trigger(this, playerInfo)

    member private this.PlayerLeaveHandler sender packet =
        let playerLeave = new PlayerLeave(packet)
        let mutable playerInfo = null
        playerInfoMap.TryGetPlayer(playerLeave.PlayerId, &playerInfo) |> ignore
        playerInfoMap.Remove(playerInfo)
        teamInfoMap.Remove(playerInfo)
        playerLeaveEvent.Trigger(this, playerInfo)

    member private this.PlayerFreqChangeHandler sender packet =
        let freqChange = new PlayerFreqChange(packet)
        let mutable playerInfo = null
        playerInfoMap.TryGetPlayer(freqChange.PlayerId, &playerInfo) |> ignore
        teamInfoMap.Remove(playerInfo)
        playerInfo.Frequency <- freqChange.Frequency
        teamInfoMap.Add(playerInfo)
        playerFreqChangeEvent.Trigger(this, playerInfo)

    member private this.PlayerShipChangeHandler sender packet =
        let shipChange = new PlayerShipChange(packet)
        let mutable playerInfo = null
        playerInfoMap.TryGetPlayer(shipChange.PlayerId, &playerInfo) |> ignore
        playerInfo.ShipType <- shipChange.ShipType
        if (playerInfo.Frequency <> shipChange.Frequency) then
            teamInfoMap.Remove(playerInfo)
            playerInfo.Frequency <- shipChange.Frequency
            teamInfoMap.Add(playerInfo)
            playerFreqChangeEvent.Trigger(this, playerInfo)
        playerShipChangeEvent.Trigger(this, playerInfo)

    member private this.PlayerScoreUpdateHandler sender packet =
        let scoreUpdate = new PlayerScoreUpdate(packet)
        let mutable playerInfo = null
        playerInfoMap.TryGetPlayer(scoreUpdate.PlayerId, &playerInfo) |> ignore
        playerInfo.Deaths <- scoreUpdate.Deaths
        playerInfo.Kills <- scoreUpdate.Kills
        playerInfo.KillPoints <- scoreUpdate.KillPoints
        playerInfo.FlagPoints <- scoreUpdate.FlagPoints
        playerScoreUpdateEvent.Trigger(this, playerInfo)


    member private this.PlayerDiedHandler sender packet =
        let playerDied = new PlayerDied(packet)
        let mutable killer = null
        let mutable killed = null
        playerInfoMap.TryGetPlayer(playerDied.KillerPlayerId, &killer) |> ignore
        playerInfoMap.TryGetPlayer(playerDied.KilledPlayerId, &killed) |> ignore
        killer.KillPoints <- killer.KillPoints + (uint32)(playerDied.BountyEarned)
        //killer.

        ()