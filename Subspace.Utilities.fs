﻿// Copyright (c) 2012 Fc <fcx.code@gmail.com>
//            All Rights Reserved
//
// This software is provided 'as-is', without any express or implied 
// warranty. In no event will the author be held liable for any damages 
// arising from the use of this software.
//
namespace EGSoft.Fc.SubspaceModel.Utilities

open System
open LanguagePrimitives

module Helper =

    let GetStaticMethod<'T when 'T :> Delegate> (typeInfo : Type, methodName : String) = 
        let methodInfo = typeInfo.GetMethod(methodName)
        Delegate.CreateDelegate(typeof<'T>, methodInfo) :?> 'T

    let ByteToHex abyte = 
        abyte
        |> (fun (x : byte) -> System.String.Format("{0:X2}", x))

    let BytesToHex bytes = 
        bytes 
        |> Array.map (fun (x : byte) -> System.String.Format("{0:X2}", x))
        |> String.concat System.String.Empty

    let UnHandledMsg (eType : Type , pType : byte, custom : string) = 
        try
            let pEnumType = Enum.Parse(eType, String.Format("{0}",pType), true)
            printf "Unhandled %s- %s %s %s\n" custom eType.FullName (System.Enum.GetName(eType, pType)) (ByteToHex pType)
        with
            | e -> (printf "Unhandled %s- %s %s" custom (ByteToHex pType) e.Message)
            


        

