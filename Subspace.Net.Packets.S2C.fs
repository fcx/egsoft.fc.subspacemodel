﻿// Copyright (c) 2012 Fc <fcx.code@gmail.com>
//            All Rights Reserved
//
// This software is provided 'as-is', without any express or implied 
// warranty. In no event will the author be held liable for any damages 
// arising from the use of this software.
//
namespace EGSoft.Fc.SubspaceModel.Net.Packets.S2C

open System
open System.Collections
open System.Collections.Generic
open LanguagePrimitives

open EGSoft.Fc.SubspaceModel
open EGSoft.Fc.SubspaceModel.Net
open EGSoft.Fc.SubspaceModel.Net.Utilities
open EGSoft.Fc.SubspaceModel.Net.Packets

type LoginResponse =
    struct
        val LoginResponseType : LoginResponseType;
        val ServerVersion : uint32
        val EXEChecksum : uint32
        val RegistrationFormRequest : bool
        val NewsChecksum : uint32

        new (loginResponseType, serverVersion, exeChecksum, registrationFormRequest, newsChecksum) =
            {   LoginResponseType = loginResponseType;
                ServerVersion = serverVersion;
                EXEChecksum = exeChecksum;
                RegistrationFormRequest = registrationFormRequest;
                NewsChecksum = newsChecksum;
            }
        new (packet : PacketS2CReader) =
            {   LoginResponseType = EnumOfValue (packet.ReadByte(1))
                ServerVersion = packet.ReadUInt32(2)
                EXEChecksum = packet.ReadUInt32(10)
                RegistrationFormRequest = if packet.ReadByte(19) <> 0uy then true else false
                NewsChecksum = packet.ReadUInt32(24)
            }
        new (buffer : byte[], index : int, size : int) =
            {   LoginResponseType = EnumOfValue buffer.[index + 1]
                ServerVersion = PacketBuilder.ReadUInt32(buffer, index + 2)
                EXEChecksum = PacketBuilder.ReadUInt32(buffer, index + 10)
                RegistrationFormRequest = if buffer.[19] <> 0uy then true else false
                NewsChecksum = PacketBuilder.ReadUInt32(buffer, index + 24)
            }
        interface IPacketS2C with
            member x.get_Type() = EnumToValue PacketS2CType.LoginResponse
    end

type ArenaEntering =
    struct
        val PlayerId : uint16
        new(packet : PacketS2CReader) =
            {   PlayerId = packet.ReadUInt16(1) }
        interface IPacketS2C with
            member  x.get_Type() = EnumToValue PacketS2CType.ArenaEntering
    end

type MessageIn =
    struct
        val MessageType : MessageType;
        val SoundType : SoundType;
        val PlayerId : uint16;
        val Message : String;
        new (messageType, soundType, playerId, message) =
            {   MessageType = messageType
                SoundType = soundType;
                PlayerId = playerId;
                Message = message;
            }
        new (packet : PacketS2CReader) =
            { 
                MessageType = EnumOfValue (packet.ReadByte(1))
                SoundType = EnumOfValue (packet.ReadByte(2))
                PlayerId = packet.ReadUInt16(3)
                Message = packet.ReadStringUntilNullTerminated(5, MessageIn.MaxSize)
            }
        static member MaxSize = 256 - 6 - 1 //server only allows max of that
        interface IPacketS2C with
            member  x.get_Type() = EnumToValue PacketS2CType.Chat
    end


type ArenaList =
    struct
        val ArenaInfoArray : ArenaInfo list
        new (packet : PacketS2CReader) =
            let index = ref 1
            {
                ArenaInfoArray = 
                    [ while !index < packet.Size do
                        let arenaName = packet.ReadStringUntilNullTerminated(!index, 16)
                        index := !index + arenaName.Length + 1
                        let arenaPop = packet.ReadInt16(!index)
                        index := !index + 2
                        yield new ArenaInfo(arenaName, (int)arenaPop)
                    ]
            }
        interface IPacketS2C with
            member  x.get_Type() = EnumToValue PacketS2CType.ArenaList
    end

type PlayersEnter =
    struct
        val PlayerInfoArray : PlayerInfo list
        new (packet : PacketS2CReader) =
            {
                PlayerInfoArray = 
                    [ for index in 0 .. 64 .. packet.Size - 64 do
                        yield new PlayerInfo(EnumOfValue (packet.ReadByte(index + 1))
                                            ,if packet.ReadByte(index + 2) <> 0uy then true else false
                                            ,packet.ReadStringUntilNullTerminated(index + 3, 20)
                                            ,packet.ReadStringUntilNullTerminated(index + 23, 20)
                                            ,packet.ReadUInt32(index + 43)
                                            ,packet.ReadUInt32(index + 47)
                                            ,packet.ReadUInt16(index + 51)
                                            ,packet.ReadUInt16(index + 53)
                                            ,packet.ReadUInt16(index + 55)
                                            ,packet.ReadUInt16(index + 57)
                                            ,packet.ReadUInt16(index + 59)
                                            ,packet.ReadUInt16(index + 61)
                                            ,if packet.ReadByte(index + 63) <> 0uy then true else false)
                    ]
            }
        interface IPacketS2C with
            member  x.get_Type() = EnumToValue PacketS2CType.PlayersEnter
    end

type PlayerLeave =
    struct
        val PlayerId : uint16
        new(packet : PacketS2CReader) =
            {   PlayerId = packet.ReadUInt16(1) }
        interface IPacketS2C with
            member  x.get_Type() = EnumToValue PacketS2CType.PlayerLeave
    end

type PlayerFreqChange =
    struct
        val PlayerId : uint16
        val Frequency : uint16
        new(packet : PacketS2CReader) =
            {   
                PlayerId = packet.ReadUInt16(1) 
                Frequency = packet.ReadUInt16(3) 
            }
        interface IPacketS2C with
                member  x.get_Type() = EnumToValue PacketS2CType.PlayerFreqChange
    end

type PlayerShipChange =
    struct
        val ShipType : ShipType;
        val PlayerId : uint16;
        val Frequency : uint16;
        new(packet : PacketS2CReader) =
            {   ShipType = EnumOfValue (packet.ReadByte(1))
                PlayerId = packet.ReadUInt16(2)
                Frequency = packet.ReadUInt16(4)
            }
        interface IPacketS2C with
                member  x.get_Type() = EnumToValue PacketS2CType.PlayerShipChange
    end

type PlayerScoreUpdate =
    struct
        val PlayerId : uint16;
        val FlagPoints : uint32;
        val KillPoints : uint32;
        val Kills : uint16;
        val Deaths : uint16;
        new(packet : PacketS2CReader) =
            {   PlayerId = packet.ReadUInt16(1)
                FlagPoints = packet.ReadUInt32(3)
                KillPoints = packet.ReadUInt32(7)
                Kills = packet.ReadUInt16(11)
                Deaths = packet.ReadUInt16(13)
            }
        interface IPacketS2C with
                member  x.get_Type() = EnumToValue PacketS2CType.PlayerScoreUpdate
    end


type PlayerDied =
    struct
        val GreenTypeId : byte;
        val KillerPlayerId : uint16;
        val KilledPlayerId : uint16;
        val BountyEarned: uint16;
        val FlagsTransferredCount : uint16;
        new(packet : PacketS2CReader) =
            {   GreenTypeId = packet.ReadByte(1)
                KillerPlayerId = packet.ReadUInt16(2)
                KilledPlayerId = packet.ReadUInt16(4)
                BountyEarned = packet.ReadUInt16(6)
                FlagsTransferredCount = packet.ReadUInt16(8)
            }
        interface IPacketS2C with
                member  x.get_Type() = EnumToValue PacketS2CType.PlayerDied
    end

type FileTransferIn =
    struct
        val Filename : String;
        val FileData : byte[]
        new (packet : PacketS2CReader) =
            {   Filename = packet.ReadStringUntilNullTerminated(1, 16)
                FileData = packet.ReadByteArray(16, packet.Size - 17);
            }
    end