﻿// Copyright (c) 2012 Fc <fcx.code@gmail.com>
//            All Rights Reserved
//
// This software is provided 'as-is', without any express or implied 
// warranty. In no event will the author be held liable for any damages 
// arising from the use of this software.
//
namespace EGSoft.Fc.SubspaceModel.Net

open EGSoft.Fc.SubspaceModel.Utilities

type PacketType =
    | PacketCore = 0x0000us
    | PacketS2C = 0x0100us
    | PacketC2S = 0x0200us
    | PacketCustom = 0x0300us

type PacketS2CType =
    | Core = 0x00uy
    | ArenaEntering = 0x01uy             //	PlayerID Change
    | ArenaEntered = 0x02uy            //	You are now in the game        
    | PlayersEnter = 0x03uy       //	Game	0x402B43	Player Entering        
    | PlayerLeave = 0x04uy        //	Game	0x402C58	 PlayerLeaving        
    | PositionWithWeaponFired = 0x05uy          //	Game	0x402D34	Player fired a weapon        
    | PlayerDied = 0x06uy           //Game	0x402CFC	Player died        
    | Chat = 0x07uy              //Chat	0x40303D	Chat        
    | Green = 0x08uy        
    | PlayerScoreUpdate = 0x09uy        
    | LoginResponse = 0x0Auy        //Game	0x40306F	Password response        
    | SoccerGoal = 0x0Buy        
    | VoiceMessage = 0x0Cuy
    | PlayerFreqChange = 0x0Duy        
    | Turret = 0x0Euy        
    | ArenaSettings = 0x0Fuy        
    | FileTransfer = 0x10uy         //* Subspace Client Does No Operation With 0x11 */        
    | FlagLocation = 0x12uy        
    | FlagPickup = 0x13uy        
    | FlagReset = 0x14uy        
    | TurretKick = 0x15uy        
    | FlagDrop = 0x16uy
    | Unknown17 = 0x17uy            //* Subspace Client Does No Operation With 0x17 */        
   
    | RequestClientFile = 0x19uy        
    | TimedGame = 0x1Auy
    | ShipReset = 0x1Buy            //* Just 1 Byteuy Tells Client They Need To Reset Their Ship */    
    | SpecData = 0x1Cuy             //* Two Bytesuy If Byte Two Is Trueuy Client Needs To Send Their Item Info In Position Packetsuy Or Three Bytesy Parameter Is The Player Id Of A Player Going Into Spectator Mode   
    | PlayerShipChange = 0x1Duy        
    | BannerToggle = 0x1Euy        
    | Banner = 0x1Fuy        
    | PrizeRecv = 0x20uy        
    | Brick = 0x21uy        
    | TurfFlags = 0x22uy        
    | PeriodicReward = 0x23uy 
    | Speed = 0x24uy                //* Complex Speed Stats */ 
    | Ufo = 0x25uy                  //* Two Bytesuy If Byte Two Is Trueuy You Can Use Ufo If You Want To */
    //* Subspace Client Does No Operation With 0x26 */        
    | KeepAlive = 0x27uy        
    | Position = 0x28uy        
    | MapFileCheck = 0x29uy        
    | MapFileCompressed = 0x2Auy     
    | KOTHSetTimer = 0x2Buy        
    | KOTHReset = 0x2Cuy
    | Unknown2D = 0x2Duy            //* Missing 2D : Some Timer Change? */        
    | Ball = 0x2Euy        
    | ArenaList = 0x2Fuy
    | Adbanner = 0x30uy             //* Vie's Old Method Of Showing Ads */
    | LoginOk = 0x31uy              //* Vie Sent It After A Good Loginuy Only With Billing. */ 
    | WarpTo = 0x32uy               //* U8 Type - Ui16 x Tile Coords - Ui16 Y Tile Coords */ 
    | LoginText = 0x33uy        
    | ContVersion = 0x34uy
    | ToggleObject = 0x35uy         //* U8 Type - Unlimited Number Of Ui16 With Obj Id (If & 0xf000uy Means Turning Off) */ 
    | MoveObject = 0x36uy
    | ToggleDamage = 0x37uy         // Two Bytesuy If Byte Two Is Trueuy Client Should Send Damage Info */ 
    | WatchDamage = 0x38uy          // Complexuy The Info Used From A *Watchdamage */     
    // Missing 39 3A        
    | Redirect = 0x3Buy
//    //CustomType
//    | Tick = 0xFCuy

type PacketC2SType =
    | Core = 0x00uy
    | ArenaEnter = 0x01uy           //	Game	0x40B750	Arena Login
    | ArenaLeave = 0x02uy           //	Game	0x40B3C1	Leave arena
    | Position = 0x03uy             //	Game	0x409BA9	Position
    | PacketTampering = 0x04uy      //	Game	0x40CAA1	Packet Tampering
    | Death = 0x05uy                //	Game	0x40A15B	Death message
    | Chat = 0x06uy                 //	Game    0x40B67C	Chat
    | TakeGreen = 0x07uy            //	Game	0x40AC3B	Take green
    | SpectateRequest = 0x08uy      //	Game	0x40B26E	Spectate request
    | LoginRequest = 0x09uy         //	Game	0x40C0E7	Password/Login
    | PacketTampering2 = 0x0Auy     //	Game	0x40CAA1	Packet tampering
    | UpdateRequest = 0x0Buy        //	Game	0x409EA9	SSUpdate.EXE request
    | MapRequest = 0x0Cuy           //	Game	0x409A6F	Map Request
    | NewsRequest = 0x0Duy          //	Game	0x409A9F	news.txt request
    | Voice = 0x0Euy                //	Game	0x40B4D7	Voice message
    | FrequencyChange = 0x0Fuy
    | AttachRequest = 0x10uy
    | FlagPickup = 0x13uy
    | FlagDrop = 0x15uy
    | FileTransfer = 0x16uy
    | RegistrationForm = 0x17uy
    | SetShipType = 0x18uy
    | SetBanner = 0x19uy
    | SecurityCheck = 0x1Auy
    | SecurityViolation = 0x1Buy
    | DropBrick = 0x1Cuy
    | KotHEndTimer = 0x1Euy
    | BallRelease = 0x1Fuy
    | BallRetreieve = 0x20uy

type PacketCoreType =            
    | Core = 0x00uy
    | EncryptionRequest = 0x01uy            
    | EncryptionResponse = 0x02uy            
    | Reliable = 0x03uy            
    | ReliableAck = 0x04uy            
    | SyncRequest = 0x05uy            
    | SyncResponse = 0x06uy            
    | Disconnect = 0x07uy            
    | SmallChunkBody = 0x08uy            
    | SmallChunkTail = 0x09uy            
    | BigChunkBody = 0x0Auy            
    | Cluster = 0x0Euy

//type MessageInOutType =
//    | Broadcast = 0x00uy
//    | PublicMacro = 0x01uy
//    | PublicMessage = 0x02uy
//    | TeamMessage = 0x03uy
//    | TeamBroadcast = 0x04uy
//    | PrivateMessage = 0x05uy
//    | ServerMessage = 0x06uy
//    | PrivateRemoteMessage = 0x07uy
//    | Warning = 0x08uy
//    | ChannelMessage = 0x09uy
