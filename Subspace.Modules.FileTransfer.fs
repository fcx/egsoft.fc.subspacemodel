﻿// Copyright (c) 2012 Fc <fcx.code@gmail.com>
//            All Rights Reserved
//
// This software is provided 'as-is', without any express or implied 
// warranty. In no event will the author be held liable for any damages 
// arising from the use of this software.
//
namespace EGSoft.Fc.SubspaceModel.Modules

open System
open System.Threading
open System.Collections.Concurrent
open System.Collections.Generic
open System.Runtime.InteropServices
open System.Text.RegularExpressions

open LanguagePrimitives

open EGSoft.Fc.EngineModel
open EGSoft.Fc.SubspaceModel
open EGSoft.Fc.SubspaceModel.Net
open EGSoft.Fc.SubspaceModel.Net.Packets
open EGSoft.Fc.SubspaceModel.Net.Packets.C2S
open EGSoft.Fc.SubspaceModel.Net.Packets.S2C

type FileDownloadedHandle = delegate of sender:obj * file:FileTransferIn -> unit
type FileUploadedHandle = delegate of sender:obj * filename:String -> unit

[<AllowNullLiteral>]
type IFileTransfer =
    inherit IFcEngineModule
    abstract Upload : filename:String * fileData:byte[]-> unit
    abstract Download : filename:String -> unit
    [<CLIEvent>]
    abstract FileUploaded : IEvent<FileUploadedHandle,_>
    [<CLIEvent>]
    abstract FileDownloaded : IEvent<FileDownloadedHandle,_>


[<AllowNullLiteral>]
[<FcEngineModuleInfo(
    ModuleName = "FileTransfer",
    Author = "Fc",
    Version = "v1.0")>]
type FileTransfer() =
    let mutable _linker = null
    let mutable _zone = null
    let mutable _message = null

    let fileUploaddedEvent = new Event<FileUploadedHandle,String>()
    let fileDownloadedEvent = new Event<FileDownloadedHandle,FileTransferIn>()

    let mutable isFileUploading = false
    let fileReceivedRegex = new Regex("File Received: (.*)")

    interface IFileTransfer with
        member this.OnLoad(linker:IFcEngineLinker) =
            _linker <- linker
            _zone <- _linker.AttachModule<IZone>()
            _zone.RecievedPacketAddHandler((byte)(EnumToValue PacketS2CType.FileTransfer), new ReceivedPacketHandle(this.Downloaded))

            _message <- _linker.AttachModule<IMessenger>()
            _message.MessageReceived.AddHandler(new MessageReceivedHandle(this.MessageReceivedHandler))
            true

        member this.OnUnload(linker:IFcEngineLinker) =
            true


        [<CLIEvent>]
        member this.FileUploaded = fileUploaddedEvent.Publish
        [<CLIEvent>]
        member this.FileDownloaded = fileDownloadedEvent.Publish

        member this.Upload(filename:String, fileData:byte[]) =
            let fileOut = new FileTransferOut(filename, fileData)
            isFileUploading <- true
            _zone.SendPacket(fileOut, true)
            ()

        member this.Download(filename:String) =
            _message.SendPublic(String.Format("*getfile {0}", filename), SoundType.None)
            ()

    member this.MessageReceivedHandler sender (msgInfo:MessageInfo) =
        if (isFileUploading) then
            let regMsg = fileReceivedRegex.Match(msgInfo.Message)
            if (regMsg.Success) then
                fileUploaddedEvent.Trigger(this, regMsg.Groups.[1].Value)
                isFileUploading <- false
        ()

    member this.Downloaded (sender:obj) (packet:PacketS2CReader) =
        let file = new FileTransferIn(packet)
        fileDownloadedEvent.Trigger(this, file)
        ()


        //packet.
//        let filename = packet.ReadStringUntilNullTerminated(1, 16)
//        let file = File.Create(filename)
//        file.Write(packet.Buffer, 17, packet.Size - 1 - 16)
//        file.Close()
//        printf "File %s Downloaded. %d %d" filename packet.Size packet.Buffer.Length
//            let stuff : byte[] = Array.zeroCreate ((int)file.Length)
//            let readNumb = file.Read(stuff, 0, (int)file.Length)

//            //let unzipper = new ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputStream(zippedData, new ICSharpCode.SharpZipLib.Zip.Compression.Inflater(false));
//            
//            let zippedFileStream = new MemoryStream()
//            let deflater = new ICSharpCode.SharpZipLib.Zip.Compression.Deflater(ICSharpCode.SharpZipLib.Zip.Compression.Deflater.DEFAULT_COMPRESSION, false)
//            let deflaterStream = new ICSharpCode.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream(zippedFileStream, deflater) :> Stream
//
//            deflaterStream.Write(stuff, 0, (int)file.Length)
//            deflaterStream.Close()
////
//            let output = zippedFileStream.ToArray()
            //let output = Array.zeroCreate 0