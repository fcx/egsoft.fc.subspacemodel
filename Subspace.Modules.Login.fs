﻿// Copyright (c) 2012 Fc <fcx.code@gmail.com>
//            All Rights Reserved
//
// This software is provided 'as-is', without any express or implied 
// warranty. In no event will the author be held liable for any damages 
// arising from the use of this software.
//
namespace EGSoft.Fc.SubspaceModel.Modules

open System
open System.Text
open LanguagePrimitives

open EGSoft.Fc.EngineModel
open EGSoft.Fc.SubspaceModel
open EGSoft.Fc.SubspaceModel.Utilities
open EGSoft.Fc.SubspaceModel.Net
open EGSoft.Fc.SubspaceModel.Net.Utilities
open EGSoft.Fc.SubspaceModel.Net.Packets
open EGSoft.Fc.SubspaceModel.Net.Packets.Core
open EGSoft.Fc.SubspaceModel.Net.Packets.C2S
open EGSoft.Fc.SubspaceModel.Net.Packets.S2C
open EGSoft.Fc.SubspaceModel.Net.Security

type LoginResponseHandle = delegate of sender:obj * loginResponseType:LoginResponseType -> unit
[<AllowNullLiteral>]
type ILogin =
    inherit IFcEngineModule
    abstract UserName : String
        with get, set
    abstract Password : String
        with get, set
    abstract ServerVersion : uint32
        with get
    abstract EXEChecksum : uint32
        with get
    abstract NewsChecksum : uint32
        with get
    [<CLIEvent>]
    abstract LoginResponse : IEvent<LoginResponseHandle,_>

[<AllowNullLiteral>]
[<FcEngineModuleInfo(
    ModuleName = "Login"
    ,Version = "v1.0"
    ,Description = "Processes Subspace authentication."
    ,Author = "Fc"
)>]
type Login() =
    let mutable serverVersion = 0u
    let mutable exeChecksum = 0u
    let mutable newsChecksum = 0u

    let mutable username = String.Empty
    let mutable password = String.Empty
    let mutable _linker:IFcEngineLinker = null
    let mutable _zone:IZone = null

    let loginResponseEvent = new Event<LoginResponseHandle,LoginResponseType>()

    interface ILogin with
        member this.OnLoad(linker:IFcEngineLinker) =
            _linker <- linker
            _zone <- linker.AttachModule<IZone>()
            _zone.Connected.AddHandler(new ZoneConnectedHandle(this.ZoneConnectedHandler))
            //_zone.Disconnected.AddHandler(new ZoneDisconnectedHandle((fun sender value -> this.ZoneDisconnectedHandler(sender, value))))
            _zone.RecievedPacketAddHandler((byte)(EnumToValue PacketS2CType.LoginResponse), new ReceivedPacketHandle(this.LoginResponseHandler))
            true
        member this.OnUnload(linker:IFcEngineLinker) =
            true


        [<CLIEvent>]
        member this.LoginResponse = loginResponseEvent.Publish

        member this.UserName
            with get() = username
            and set v = username <- v

        member this.Password
            with get() = password
            and set v = password <- v

        member this.ServerVersion = serverVersion
        member this.EXEChecksum = exeChecksum
        member this.NewsChecksum = newsChecksum

    member private this.LoginResponseHandler sender packet =
        let loginResponse = new LoginResponse(packet)
        if (loginResponse.RegistrationFormRequest) then
            _zone.SendPacket(new RegistrationForm(   
                                    username, 
                                    "fcx.code@gmail.com", 
                                    "Extreme Games", 
                                    "Subspace", 
                                    RegistrationFormGenderType.Secret, 
                                    0uy), true)

        serverVersion <- loginResponse.ServerVersion
        exeChecksum <- loginResponse.EXEChecksum
        newsChecksum <- loginResponse.ServerVersion
                                
        match loginResponse.LoginResponseType with
        | LoginResponseType.RegistrationRequired ->
            //printf "Unregistered Player"
            _zone.SendPacket(new LoginRequest(username, password, true), true)
        //| LoginResponseType.Success -> connectedEvent.Trigger(this, true)
        |_ -> 
            Helper.UnHandledMsg(typeof<LoginResponseType>, EnumToValue loginResponse.LoginResponseType, "LoginResponse")
            loginResponseEvent.Trigger(this, loginResponse.LoginResponseType)
        ()

    member private this.ZoneConnectedHandler sender canConnect =
        if canConnect then
            _zone.SendPacket(new LoginRequest(username, password, false), true)


//    member private this.ZoneDisconnectedHandler(sender : obj, args : bool)  =
//        //Auto relogin
//        System.Threading.Thread.Sleep(10000)
//        printf "Reconnecting...."
//        _zone.Connect(loginAddressIP, loginAddressPort) |> ignore
//        ()