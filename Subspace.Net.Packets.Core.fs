﻿// Copyright (c) 2012 Fc <fcx.code@gmail.com>
//            All Rights Reserved
//
// This software is provided 'as-is', without any express or implied 
// warranty. In no event will the author be held liable for any damages 
// arising from the use of this software.
//
namespace EGSoft.Fc.SubspaceModel.Net.Packets.Core

open System
open LanguagePrimitives
open System.Collections.Generic

open EGSoft.Fc.SubspaceModel
open EGSoft.Fc.SubspaceModel.Net
open EGSoft.Fc.SubspaceModel.Net.Utilities
open EGSoft.Fc.SubspaceModel.Net.Packets


type Disconnect =
    struct
        val Message : String
        new(message) = 
            { Message = message; }
        interface IPacketCore with
            member x.get_Type() = EnumToValue PacketCoreType.Core
            member x.get_Size() = 2
            member this.Serialize(buffer : byte[], index : int) = 
                //buffer.[index + 0] <- EnumToValue PacketCoreType.Core
                buffer.[index + 1] <- EnumToValue PacketCoreType.Disconnect
                (this :> IPacketC2S).Size
    end

type EncryptionRequest =
    struct
        val ClientKey : uint32

        new(clientKey) =
            { ClientKey = clientKey
            }
        new(buffer : byte[], index : int) =
            { ClientKey = PacketBuilder.ReadUInt32(buffer, index + 2); 
            }
        interface IPacketCore with
            member x.get_Type() = EnumToValue PacketCoreType.Core
            member x.get_Size() = 8
            member this.Serialize(buffer : byte[] , index : int) = 
                //buffer.[index + 0] <- EnumToValue PacketCoreType.Core
                buffer.[index + 1] <- EnumToValue PacketCoreType.EncryptionRequest
                PacketBuilder.WriteUInt32(buffer, index + 2, this.ClientKey) |> ignore
                buffer.[index + 6] <- 0x01uy
                buffer.[index + 7] <- 0x00uy
                8
    end
   
type EncryptionResponse =
    struct
        val ServerKey : uint32

        new(serverKey) =
            { ServerKey = serverKey
            }
        new(buffer : byte[], index : int, size : int) =
            { ServerKey = PacketBuilder.ReadUInt32(buffer, index + 2); 
            }
        interface IPacketCore with
            member x.get_Type() = EnumToValue PacketCoreType.Core
            member x.get_Size() = 6
            member this.Serialize(buffer : byte[], index : int) = 
                //buffer.[index + 0] <- EnumToValue PacketCoreType.Core
                buffer.[index + 1] <- EnumToValue PacketCoreType.EncryptionResponse
                PacketBuilder.WriteUInt32(buffer, index + 2, this.ServerKey) |> ignore
                6
    end


type SyncRequest =
    struct
        val Timestamp : int32
        val TotalPacketsSent : uint32
        val TotalPacketsReceived : uint32

        new (timestamp, totalPacketsSent, totalPacketsRecieved) =
            {   Timestamp = timestamp;
                TotalPacketsSent = totalPacketsSent;
                TotalPacketsReceived = totalPacketsRecieved }
        new (buffer : byte[], index : int, size : int) = 
            if (size = 6) then
                {   Timestamp = PacketBuilder.ReadInt32(buffer, index + 2);
                    TotalPacketsSent = 0u;
                    TotalPacketsReceived = 0u;  }
            else
                {   Timestamp = PacketBuilder.ReadInt32(buffer, index + 2);
                    TotalPacketsSent = PacketBuilder.ReadUInt32(buffer, index + 6);
                    TotalPacketsReceived = PacketBuilder.ReadUInt32(buffer, index + 10);   }
        interface IPacketCore with
            member x.get_Type() = EnumToValue PacketCoreType.Core
            member x.get_Size() = 14
            member this.Serialize(buffer : byte[], index : int) = 
                //buffer.[index + 0] <- EnumToValue PacketCoreType.Core
                buffer.[index + 1] <- EnumToValue PacketCoreType.SyncRequest
                PacketBuilder.WriteInt32(buffer, index + 2, this.Timestamp) |> ignore
                PacketBuilder.WriteUInt32(buffer, index + 6, this.TotalPacketsSent) |> ignore
                PacketBuilder.WriteUInt32(buffer, index + 10, this.TotalPacketsReceived) |> ignore
                (this :> IPacketC2S).Size
    end

type SyncResponse =
    struct
        val ReceivedTimestamp : int32
        val LocalTimestamp : int32

        new (receivedTimestamp, localTimestamp) =
            {   ReceivedTimestamp = receivedTimestamp;
                LocalTimestamp = localTimestamp; }
        new (buffer : byte[], index : int, size : int) = 
            {   ReceivedTimestamp = PacketBuilder.ReadInt32(buffer, index + 2)
                LocalTimestamp = PacketBuilder.ReadInt32(buffer, index + 6)   }
        interface IPacketCore with
            member x.get_Type() = EnumToValue PacketCoreType.Core
            member x.get_Size() = 10
            member this.Serialize(buffer : byte[], index : int) = 
                //buffer.[index + 0] <- EnumToValue PacketCoreType.Core
                buffer.[index + 1] <- EnumToValue PacketCoreType.SyncResponse
                PacketBuilder.WriteInt32(buffer, index + 2, this.ReceivedTimestamp) |> ignore
                PacketBuilder.WriteInt32(buffer, index + 6, this.LocalTimestamp) |> ignore
                (this :> IPacketC2S).Size
    end


type Reliable = 
    struct
        val ReliableId : int32
        val Packet : IPacketC2S
        val mutable Timestamp : int32;

        new (reliableId, packet) =
            {   ReliableId = reliableId;
                Packet = packet;
                Timestamp = 0
            }
        static member HeaderSize = 6
        interface IPacketCore with
            member x.get_Type() = EnumToValue PacketCoreType.Core
            member this.get_Size() = this.Packet.Size + Reliable.HeaderSize
            member this.Serialize(buffer : byte[] , index : int) =
                //buffer.[index + 0] <- EnumToValue PacketCoreType.Core
                buffer.[index + 1] <- EnumToValue PacketCoreType.Reliable
                PacketBuilder.WriteInt32(buffer, index + 2, this.ReliableId) |> ignore
                this.Packet.Serialize(buffer, index + Reliable.HeaderSize) |> ignore
                buffer.[index + Reliable.HeaderSize] <- this.Packet.Type
                (this :> IPacketC2S).Size
    end

type ReliableIn =
    struct
        val Buffer : byte[]
        val Index : int
        val Size : int
        new(buffer, index, size) =
            {
                Buffer = buffer;
                Index = index;
                Size = size;
            }
    end

type ReliableAck =
    struct
        val ReliableId : int32

        new(reliableId) =
            { ReliableId = reliableId
            }
        new(buffer : byte[], index : int, size : int) =
            { ReliableId = PacketBuilder.ReadInt32(buffer, index + 2); 
            }
        interface IPacketCore with
            member x.get_Type() = EnumToValue PacketCoreType.Core
            member x.get_Size() = 6
            member this.Serialize(buffer : byte[] , index : int) = 
                buffer.[index + 0] <- EnumToValue PacketCoreType.Core
                buffer.[index + 1] <- EnumToValue PacketCoreType.ReliableAck
                PacketBuilder.WriteInt32(buffer, index + 2, this.ReliableId) |> ignore
                6
    end
                

type BigChunkBody =
    struct
        val Index: int32
        val Size: int32
        val Buffer : byte[]
        val TotalLength : int32
        
        new (totalLength, buffer, index, size) =
            {   TotalLength = totalLength; 
                Buffer = buffer;
                Index = index;
                Size = size;
            }
        interface IPacketCore with
            member this.get_Type() = EnumToValue PacketCoreType.Core
            member this.get_Size() = 2 + 4 + this.Size
            member this.Serialize(buffer : byte[] , index : int) =
                buffer.[index + 0] <- EnumToValue PacketCoreType.Core
                buffer.[index + 1] <- EnumToValue PacketCoreType.BigChunkBody
                PacketBuilder.WriteInt32(buffer, index + 2, this.TotalLength) |> ignore
                PacketBuilder.WriteByteArray(buffer, index + 6, this.Buffer,this.Index, this.Size) |> ignore
                (this :> IPacketC2S).Size
    end

type SmallChunkBody =
    struct
        val Index: int32
        val Length: int32
        val Buffer : byte[]

        new (buffer, index, length) =
            {
                Buffer = buffer;
                Index = index;
                Length = length
            }
        interface IPacketCore with
            member x.get_Type() = EnumToValue PacketCoreType.Core
            member x.get_Size() = 2 + x.Length
            member this.Serialize(buffer : byte[] , index : int) = 
                buffer.[index + 0] <- EnumToValue PacketCoreType.Core
                buffer.[index + 1] <- EnumToValue PacketCoreType.SmallChunkBody
                PacketBuilder.WriteByteArray(buffer, index + 2, this.Buffer, this.Index, this.Length) |> ignore
                (this :> IPacketC2S).Size
    end


type SmallChunkTail =
    struct
        val Index: int32
        val Length: int32
        val Buffer : byte[]

        new (buffer, index, size) =
            {   Buffer = buffer;
                Index = index;
                Length = size
            }
        interface IPacketCore with
            member x.get_Type() = EnumToValue PacketCoreType.Core
            member x.get_Size() = (2 + x.Length)
            member this.Serialize(buffer : byte[] , index : int) = 
                buffer.[index + 0] <- EnumToValue PacketCoreType.Core
                buffer.[index + 1] <- EnumToValue PacketCoreType.SmallChunkTail
                PacketBuilder.WriteByteArray(buffer, index + 2, this.Buffer, this.Index, this.Length) |> ignore
                (this :> IPacketC2S).Size
    end



//type RecievedPacketHandler = delegate of obj * PacketReader -> unit
//type PacketS2CHandler =
//    {PacketS2CHandler : Event<RecievedPacketHandler, PacketReader>; Sender : obj; PacketS2C : PacketReader}
//    interface IPacketHandler with
//        member this.Trigger() = this.PacketS2CHandler.Trigger(this.Sender, this.PacketS2C)
//    
//
//
//type RecievedTickHandler = delegate of obj * uint32 -> unit
//type internal TickHandler =
//    {TickHandler : Event<RecievedTickHandler, uint32>; Sender : obj}
//    interface IPacketHandler with
//        member this.Trigger() = this.TickHandler.Trigger(this.Sender, Timer.GetTickCount)