﻿// Copyright (c) 2012 Fc <fcx.code@gmail.com>
//            All Rights Reserved
//
// This software is provided 'as-is', without any express or implied 
// warranty. In no event will the author be held liable for any damages 
// arising from the use of this software.
//
namespace EGSoft.Fc.SubspaceModel.Net.Utilities

open System
open LanguagePrimitives

type PacketBuilder() =
    static let StringEncoder = System.Text.Encoding.GetEncoding("ISO-8859-1")
    static member WriteInt64( buffer : byte[], index : int, value : int64) =
        buffer.[index] <- ((byte)(value))
        buffer.[index+1] <- ((byte)(value>>>8))
        buffer.[index+2] <- ((byte)(value>>>16))
        buffer.[index+3] <- ((byte)(value>>>24))
        buffer.[index+4] <- ((byte)(value>>>32))
        buffer.[index+5] <- ((byte)(value>>>40))
        buffer.[index+6] <- ((byte)(value>>>48))
        buffer.[index+7] <- ((byte)(value>>>56))
    static member WriteUInt64( buffer : byte[], index : int, value : uint64) =
        buffer.[index] <- ((byte)(value))
        buffer.[index+1] <- ((byte)(value>>>8))
        buffer.[index+2] <- ((byte)(value>>>16))
        buffer.[index+3] <- ((byte)(value>>>24))
        buffer.[index+4] <- ((byte)(value>>>32))
        buffer.[index+5] <- ((byte)(value>>>40))
        buffer.[index+6] <- ((byte)(value>>>48))
        buffer.[index+7] <- ((byte)(value>>>56))
    static member WriteInt32( buffer : byte[], index : int, value : int32) =
        buffer.[index] <- ((byte)(value))
        buffer.[index+1] <- ((byte)(value>>>8))
        buffer.[index+2] <- ((byte)(value>>>16))
        buffer.[index+3] <- ((byte)(value>>>24))
    static member WriteUInt32( buffer : byte[], index : int, value : uint32) =
        buffer.[index] <- ((byte)(value))
        buffer.[index+1] <- ((byte)(value>>>8))
        buffer.[index+2] <- ((byte)(value>>>16))
        buffer.[index+3] <- ((byte)(value>>>24))
    static member WriteInt16( buffer : byte[], index : int, value : int16) =
        buffer.[index] <- ((byte)(value))
        buffer.[index+1] <- ((byte)(value>>>8))
    static member WriteUInt16( buffer : byte[], index : int, value : uint16) =
        buffer.[index] <- ((byte)(value))
        buffer.[index+1] <- ((byte)(value>>>8))
//    static member WriteSByte( buffer : byte[], index : int, value : sbyte) =
//        buffer.[index] <- ((byte)(value))
//    static member WriteByte( buffer : byte[], index : int, value : byte) =
//        buffer.[index] <- value

    static member WriteByteArray( buffer : byte[], index : int, value : byte[], valueIndex : int, valueLength : int) =
        Array.Copy(value, valueIndex, buffer, index, valueLength)


    static member WriteString( buffer : byte[], index : int, value : String) =
        let stringBytes = StringEncoder.GetBytes(value)
        let stringBytesLength = stringBytes.Length
        Array.Copy(stringBytes, 0, buffer, index, stringBytesLength)
        buffer.[index + stringBytesLength] <- 0uy
        stringBytesLength

    static member WriteString( buffer : byte[], index : int, value : String, valueLength : int) =
        let stringBytes = StringEncoder.GetBytes(value)
        let minStrLen = Math.Min(stringBytes.Length, valueLength)
        Array.Copy(stringBytes, 0, buffer, index, minStrLen)
        buffer.[index + minStrLen] <- 0uy


    static member ReadInt64( buffer : byte[], index : int) =
        (((int64)(buffer.[index+7]))<<<56) |||
        (((int64)(buffer.[index+6]))<<<48) |||
        (((int64)(buffer.[index+5]))<<<40) ||| 
        (((int64)(buffer.[index+4]))<<<32) |||
        (((int64)(buffer.[index+3]))<<<24) |||
        (((int64)(buffer.[index+2]))<<<16) ||| 
        (((int64)(buffer.[index+1]))<<<8)  |||
        (((int64)(buffer.[index])))

    static member ReadUInt64( buffer : byte[], index : int) =
        (((uint64)(buffer.[index+7]))<<<56) |||
        (((uint64)(buffer.[index+6]))<<<48) |||
        (((uint64)(buffer.[index+5]))<<<40) ||| 
        (((uint64)(buffer.[index+4]))<<<32) |||
        (((uint64)(buffer.[index+3]))<<<24) |||
        (((uint64)(buffer.[index+2]))<<<16) ||| 
        (((uint64)(buffer.[index+1]))<<<8)  |||
        (((uint64)(buffer.[index])))

    static member ReadInt32( buffer : byte[], index : int) =
        (((int32)(buffer.[index+3]))<<<24) |||
        (((int32)(buffer.[index+2]))<<<16) ||| 
        (((int32)(buffer.[index+1]))<<<8)  |||
        (((int32)(buffer.[index])))

    static member ReadInt32( buffer : byte[], index : int byref) =
        index <- 4 + index 
        (((int32)(buffer.[index-4]))<<<24) |||
        (((int32)(buffer.[index-3]))<<<16) ||| 
        (((int32)(buffer.[index-2]))<<<8)  |||
        (((int32)(buffer.[index-1])))

    static member ReadUInt32( buffer : byte[], index : int byref) =
        index <- 4 + index
        (((uint32)(buffer.[index-1]))<<<24) |||
        (((uint32)(buffer.[index-2]))<<<16) ||| 
        (((uint32)(buffer.[index-3]))<<<8)  |||
        (((uint32)(buffer.[index-4]))) 

    static member ReadUInt32( buffer : byte[], index : int) =
        (((uint32)(buffer.[index+3]))<<<24) |||
        (((uint32)(buffer.[index+2]))<<<16) ||| 
        (((uint32)(buffer.[index+1]))<<<8)  |||
        (((uint32)(buffer.[index])))

    static member ReadInt16( buffer : byte[], index : int) =
        (((int16)(buffer.[index+1]))<<<8)  |||
        (((int16)(buffer.[index])))

    static member ReadInt16( buffer : byte[], index : int byref) =
        let temp = index
        index <- index + 2
        (((int16)(buffer.[temp+1]))<<<8)  |||
        (((int16)(buffer.[temp])))

    static member ReadUInt16( buffer : byte[], index : int) =
        (((uint16)(buffer.[index+1]))<<<8)  |||
        (((uint16)(buffer.[index])))

    static member ReadUInt16( buffer : byte[], index : int byref) =
        let temp = index
        index <- index + 2
        (((uint16)(buffer.[temp+1]))<<<8)  |||
        (((uint16)(buffer.[temp])))

//    static member ReadSByte( buffer : byte[], index : int) =
//        (((sbyte)(buffer.[index])))
//
//    static member ReadByte( buffer : byte[], index : int) =
//        (((buffer.[index])))
//
//    static member ReadByte( buffer : byte[], index : int byref) =
//        let temp = index
//        index <- index + 1
//        (((buffer.[temp])))
            
    static member ReadByteArray( buffer : byte[], index : int byref, size : int) =
        let temp = index
        index <- index + size
        buffer.[temp .. temp + size - 1]

    static member ReadByteArray( buffer : byte[], index : int, size : int) =
        buffer.[index .. index + size - 1]

    static member ReadString( buffer : byte[], index : int, size : int) =
        StringEncoder.GetString(buffer, index, size)

    static member ReadStringUntilNullTerminated( buffer : byte[], index : int, size : int) =
        let mutable nullCount = index
        while buffer.[nullCount] <> 0uy && nullCount < index + size do
            nullCount <- nullCount + 1
        StringEncoder.GetString(buffer, index, nullCount - index)

//    static member ReadStringUntilNullTerminated( buffer : byte[], index : int, size : int) =
//        let mutable temp = index
//        while buffer.[index] <> 0uy do
//            temp <- temp + 1
//        StringEncoder.GetString(buffer, index, temp - index)
        

type PacketReader(byte : byte[]) =
    let buffer : byte[] = byte

    member this.ReadByte(index : int) = buffer.[index]
    member this.ReadUInt16(index : int) = PacketBuilder.ReadUInt16(buffer, index)
    member this.ReadInt16(index : int) = PacketBuilder.ReadInt16(buffer, index)
    member this.ReadInt16(index : int byref) = PacketBuilder.ReadInt16(buffer, &index) 
    member this.ReadUInt32(index : int) = PacketBuilder.ReadUInt32(buffer, index)
    member this.ReadInt32(index : int) = PacketBuilder.ReadInt32(buffer, index)
    member this.ReadStringUntilNullTerminated(index : int, length : int) = PacketBuilder.ReadStringUntilNullTerminated(buffer, index, length)
    member this.ReadByteArray(index : int, size : int) = PacketBuilder.ReadByteArray(buffer, index, size) 