﻿// Copyright (c) 2012 Fc <fcx.code@gmail.com>
//            All Rights Reserved
//
// This software is provided 'as-is', without any express or implied 
// warranty. In no event will the author be held liable for any damages 
// arising from the use of this software.
//
namespace EGSoft.Fc.SubspaceModel.Modules

open System
open System.Threading
open System.Collections.Concurrent
open System.Collections.Generic
open System.Runtime.InteropServices

open System.Net
open System.Net.Sockets

open LanguagePrimitives

open EGSoft.Fc.EngineModel
open EGSoft.Fc.SubspaceModel
open EGSoft.Fc.SubspaceModel.Utilities
open EGSoft.Fc.SubspaceModel.Net
open EGSoft.Fc.SubspaceModel.Net.Utilities
open EGSoft.Fc.SubspaceModel.Net.Packets
open EGSoft.Fc.SubspaceModel.Net.Packets.C2S
open EGSoft.Fc.SubspaceModel.Net.Packets.Core
open EGSoft.Fc.SubspaceModel.Net.Packets.Core.Utilities
open EGSoft.Fc.SubspaceModel.Net.Security

type ZoneState =
    | Connected = 0
    | Disconnected = 1
    | Connecting = 2
    | Disconnecting = 3 


type ZoneConnectedHandle = delegate of sender:obj * canConnect:bool-> unit
type ZoneDisconnectedHandle = delegate of sender:obj * lostConnection:bool -> unit
type ReceivedPacketHandle = delegate of sender:obj * packet:PacketS2CReader -> unit

[<AllowNullLiteral>]
type IZone =
    inherit IFcEngineModule
    abstract Connect : unit -> ZoneState
    abstract Connect : address:String * port:int -> ZoneState
    abstract Disconnect : unit -> ZoneState
    abstract SendPacket : packet:IPacketC2S * isReliable:bool -> unit
    abstract RecievedPacketAddHandler : packetType:byte * handler:ReceivedPacketHandle -> unit
    abstract RecievedPacketRemoveHandler : packetType:byte * handler:ReceivedPacketHandle -> unit
    abstract IPAddress : String
        with get, set
    abstract Port : int
        with get, set
    [<CLIEvent>]
    abstract Connected : IEvent<ZoneConnectedHandle,_>
    [<CLIEvent>]
    abstract Disconnected : IEvent<ZoneDisconnectedHandle,_>

[<AllowNullLiteral>]
[<FcEngineModuleInfo(
    ModuleName = "Zone"
    ,Version = "v1.0"
    ,Description = "Connects to a Subspace zone."
    ,Author = "Fc"
)>]
type Zone() as this =

    let encryptionMode = EncryptionMode.On
    
    let smallChunkManager = new SmallChunkManager()
    let bigChunkManager = new BigChunkManager()
    
    let mutable zoneState = ZoneState.Disconnected
    let mutable packetEncryption = new Encryption()

    let mutable clusterNotFull = true
    let mutable outGoingPacket0 = null : IPacketC2S
    let mutable outGoingPacket = null : IPacketC2S
    let mutable outGoingPacketSize = 0
    let mutable outGoingPacketClusteredSize = 0
    let mutable outGoingPacketsCount = 0
    let outGoingNormalPriorityQueue = new ConcurrentQueue<IPacketC2S>()
    let outGoingHighPriorityQueue = new ConcurrentQueue<IPacketC2S>()
    let outGoingQueueEvent = new AutoResetEvent(false)
    let receivedPacketEventMap = new ConcurrentDictionary<byte, Event<ReceivedPacketHandle,PacketS2CReader>>()

    let mutable reliablePacketsSentId = -1
    let mutable reliablePacketsReceivedId = 0
    let reliablePacketsSent_InTransit = new ConcurrentDictionary<int32, Reliable>()
    let reliablePacketsRecieved_InTransit = new ConcurrentDictionary<int32, PacketS2CReader>()

    let mutable totalPacketsSent = 0u
    let mutable totalPacketsReceived = 0u
    let mutable lastPacketSentTime = 0
    let mutable lastPacketReceivedTime = 0

    let mutable lastSyncRequestSent = 0
    let mutable lastSyncResponseReceived = 0
    let mutable lastSyncRoundTripTime = 0
   
    let mutable eXEChecksum = 0u
    let mutable settingChecksum = 0u
    let mutable mapChecksum = 0u
    let mutable weaponCount = 0u
    let mutable slowFPS = false

    let mutable pingSumOfRoundTripTime = 0
    let mutable pingRoundTripCount = 0
    let mutable pingTimeDiff = 0

    let mutable ping = 0
    let mutable pingAverage = 10
    let mutable pingLow = 0
    let mutable pingHigh = 0

    let mutable _ipAddress = String.Empty
    let mutable _port = 0

    //let mutable socket : Socket = null
    let mutable packetS2CReader = new PacketS2CReader()
    
    let mutable socketIsNew = false
    let mutable receivedState = false
    let mutable sendState = false
    let socket = new Socket(((new IPEndPoint(IPAddress.Any, 0)).AddressFamily), SocketType.Dgram, ProtocolType.Udp)
    let receivedSocketAsycEventArgs = new SocketAsyncEventArgs()
    do receivedSocketAsycEventArgs.SetBuffer(Array.zeroCreate 520, 0, 520)
    do receivedSocketAsycEventArgs.Completed.AddHandler(new EventHandler<SocketAsyncEventArgs>(fun sender args -> this.SocketReceivedPacketComplete(sender :?> Socket, args)))

    let sendSocketAsycEventArgs = new SocketAsyncEventArgs()
    do sendSocketAsycEventArgs.SetBuffer(Array.zeroCreate 520, 0, 520)
    do sendSocketAsycEventArgs.Completed.AddHandler(new EventHandler<SocketAsyncEventArgs>(fun sender args -> this.SocketSendPacketComplete(sender :?> Socket, args)))   

    let connectedEvent = new Event<ZoneConnectedHandle,_>()
    let disconnectedEvent = new Event<ZoneDisconnectedHandle,_>()

    let mutable _linker:IFcEngineLinker = null

    interface IZone with
        member this.OnLoad(linker:IFcEngineLinker) =
            _linker <- linker
            _linker.Tick.Add(this.TickHandler)
            true

        member this.OnUnload(linker:IFcEngineLinker) =
            true

        [<CLIEvent>]
        member this.Connected = connectedEvent.Publish
        [<CLIEvent>]
        member this.Disconnected = disconnectedEvent.Publish

        member this.IPAddress
            with get() = _ipAddress
            and set v = _ipAddress <- v

        member this.Port
            with get() = _port
            and set v = _port <- v

        member this.Connect(ipAddress : String, port : int) =
            _ipAddress <- ipAddress
            _port <- port
            (this :> IZone).Connect()

        member this.Connect() =
            (this :> IZone).Disconnect() |> ignore
            if (zoneState = ZoneState.Disconnected) then
                socket.Connect(_ipAddress, _port)
                if (socket.Connected) then
                    if (not sendState) then
                        sendState <- socket.SendAsync(sendSocketAsycEventArgs)
                    if (not receivedState) then
                        receivedState <- socket.ReceiveAsync(receivedSocketAsycEventArgs)
                    lastPacketReceivedTime <- _linker.TickElapsed
                    zoneState <- ZoneState.Connecting
                    this.SendPacketHighPriority(new EncryptionRequest(packetEncryption.ClientKey))
                else
                    this.Disconnected(true)
            zoneState
                

        member this.Disconnect() = 
            if (zoneState <> ZoneState.Disconnected) then
                zoneState <- ZoneState.Disconnecting
                this.SendPacketNormalPriority(new Disconnect())
            zoneState


        member this.SendPacket(packet : IPacketC2S, isReliable : bool) = 
//            if (packet.Type = EnumToValue PacketCoreType.Core) then
//                raise (new System.Exception("Error: Cant send core packets."))

            if (zoneState = ZoneState.Connected) then
                if (packet.Size <= Packet.MaxSize && not isReliable) then
                    outGoingNormalPriorityQueue.Enqueue(packet)
                else if (isReliable && (packet.Size + Reliable.HeaderSize <= Packet.MaxSize)) then
                    let reliableId = Interlocked.Increment(&reliablePacketsSentId)
                    let mutable reliablePacket = new Reliable(reliableId, packet)
                    outGoingNormalPriorityQueue.Enqueue(reliablePacket)
                    //reliablePacket.Timestamp <- _linker.TickElapsed
                    reliablePacketsSent_InTransit.TryAdd(reliableId, reliablePacket) |> ignore
                else
                    let oversizedPacketAsync =
                        async {
                            if (true || packet.Size <= 1500) then
                                let smallChunkBodies, smallChunkTail = smallChunkManager.BreakUp(packet)
                                for smb in smallChunkBodies do
                                    let reliableId = Interlocked.Increment(&reliablePacketsSentId)
                                    if (reliableId % 2 = 0) then
                                        Thread.Sleep(5)
                                    let mutable reliablePacket = new Reliable(reliableId, smb)
                                    outGoingNormalPriorityQueue.Enqueue(reliablePacket)
                                    //reliablePacket.Timestamp <- _linker.TickElapsed
                                    if (not (reliablePacketsSent_InTransit.TryAdd(reliableId, reliablePacket))) then
                                        printf "FAILED>"
                                    outGoingQueueEvent.Set() |> ignore
                                let reliableId = Interlocked.Increment(&reliablePacketsSentId)
                                let mutable reliablePacket = new Reliable(reliableId, smallChunkTail)
                                outGoingNormalPriorityQueue.Enqueue(reliablePacket)
                                //reliablePacket.Timestamp <- _linker.TickElapsed
                                if (not (reliablePacketsSent_InTransit.TryAdd(reliableId, reliablePacket))) then
                                    printf "FAILED>"
                                outGoingQueueEvent.Set() |> ignore
                            else
                                let packets = bigChunkManager.BreakUp(packet)
                                for p in packets do
                                    Thread.Sleep(10)
                                    let reliableId = Interlocked.Increment(&reliablePacketsSentId)
                                    let mutable reliablePacket = new Reliable(reliableId, p)
                                    outGoingNormalPriorityQueue.Enqueue(reliablePacket)
                                    reliablePacket.Timestamp <- _linker.TickElapsed
                                    reliablePacketsSent_InTransit.TryAdd(reliableId, reliablePacket) |> ignore
                                    outGoingQueueEvent.Set() |> ignore
                        }
                    let oversized = Async.StartAsTask(oversizedPacketAsync)
                    ()
                outGoingQueueEvent.Set() |> ignore

        member this.RecievedPacketAddHandler(packetS2CType : byte, handler : ReceivedPacketHandle) =
            let receivedPacketEvent = receivedPacketEventMap.GetOrAdd(packetS2CType, new Event<ReceivedPacketHandle,PacketS2CReader>())
            receivedPacketEvent.Publish.AddHandler(handler)  
            ()

        member this.RecievedPacketRemoveHandler(packetS2CType : byte, handler : ReceivedPacketHandle) =
            let receivedPacketEvent = receivedPacketEventMap.GetOrAdd(packetS2CType, new Event<ReceivedPacketHandle,PacketS2CReader>())
            receivedPacketEvent.Publish.RemoveHandler(handler)  
            ()

    member private this.Connected(canConnect : bool) =
        if (zoneState <> ZoneState.Connected) then
            zoneState <- ZoneState.Connected
            _linker.QueueTask(new ConnectedTask(connectedEvent, this, canConnect))

    member private this.Disconnected(lostConnection : bool) =
        if (zoneState <> ZoneState.Disconnected) then
            this.Reset()
            zoneState <- ZoneState.Disconnected
            _linker.QueueTask(new DisconnectedTask(disconnectedEvent, this, lostConnection))

    member this.TickHandler(tickElapsed : int32) =
        if (zoneState <> ZoneState.Disconnected) then
            //let tmpLastPacketReceivedTime = Interlocked.Read(&lastPacketReceivedTime)
            let lastRecevPacketDiff = tickElapsed - lastPacketReceivedTime
            //if (lastRecevPacketDiff > 15000L) then
            if (lastRecevPacketDiff > 15000) then
               // printf "NOT OKAY %d - %d\n" timeElapsed lastRecevPacketDiff
                this.Disconnected(true)
            //else
                //printf "OKAY %d - %d\n" timeElapsed lastRecevPacketDiff

            if (zoneState = ZoneState.Connected) then
                let clientTimeNow = ((tickElapsed)/10)
                if ((clientTimeNow - lastSyncRequestSent) > 250 || lastSyncRequestSent < 250) then
                    lastSyncRequestSent <- clientTimeNow
                    this.SendPacketHighPriority(new SyncRequest(clientTimeNow, totalPacketsSent, totalPacketsReceived))

                let reliablePacketReSendArray = 
                    reliablePacketsSent_InTransit.ToArray()
                    |> Array.filter (fun kv -> (kv.Value.Timestamp <> 0 && (tickElapsed - kv.Value.Timestamp) > (pingAverage * 20)))
                
                for i in 0 .. reliablePacketReSendArray.Length - 1 do
                    let mutable packet = reliablePacketReSendArray.[i].Value
                    packet.Timestamp <- tickElapsed
                    this.SendPacketHighPriority(packet)
                    printf "Resending Reliable Packet %d\n" packet.ReliableId
        ()

    member private this.SendPacketHighPriority(packet : IPacketCore) = 
        outGoingHighPriorityQueue.Enqueue(packet)
        outGoingQueueEvent.Set() |> ignore
        ()

    member private this.SendPacketNormalPriority(packet : IPacketCore) = 
        outGoingNormalPriorityQueue.Enqueue(packet)
        outGoingQueueEvent.Set() |> ignore
        ()
                
    member private this.SocketSendPacketComplete(sender : Socket, args : SocketAsyncEventArgs) =
        try
            //printf "SocketSendPacketComplete\n"
            if (zoneState <> ZoneState.Disconnected) then
                if (args.SocketError = SocketError.Success) then
                    Thread.Sleep(25)
                    this.GenerateOutgoingPackets(args.Buffer)
                    while(outGoingPacketSize = 0 && zoneState <> ZoneState.Disconnecting) do
                        outGoingQueueEvent.WaitOne() |> ignore
                        try
                            this.GenerateOutgoingPackets(args.Buffer)
                        with
                        | ex -> Console.WriteLine ex.Message
                    if (outGoingPacketSize <> 0) then
                        packetEncryption.EncryptData(args.Buffer, outGoingPacketSize)
                        args.SetBuffer(0, outGoingPacketSize)
                        totalPacketsSent <- totalPacketsSent + 1u
                        lastPacketSentTime <- _linker.TickElapsed
                        sendState <- sender.SendAsync(args)
                        //printf "Sending %s %s %s %d \n" (Helper.ByteToHex args.Buffer.[0]) (Helper.ByteToHex args.Buffer.[1]) (Helper.ByteToHex args.Buffer.[6]) outGoingPacketSize
                    else
                        sendState <- false
                        this.Disconnected(false)
                else
                    printf "SocketFailed: SocketReceivedPacketComplete\n"
            else
                sendState <- false
        with
        | ex -> Console.WriteLine ex.Message

    member private this.SocketReceivedPacketComplete(sender : Socket, args : SocketAsyncEventArgs) =
        try
            //printf "SocketReceivedPacketComplete\n"
            if (args.SocketError = SocketError.Success) then
                totalPacketsReceived <- totalPacketsReceived + 1u
                let tmpLastPacketReceivedTime = Interlocked.Exchange(&lastPacketReceivedTime, _linker.TickElapsed)
                packetEncryption.DecryptData(args.Buffer, args.BytesTransferred)
                this.ProcessIncomingPackets(args.Buffer, 0, args.BytesTransferred)
                if (zoneState <> ZoneState.Disconnected) then
                    args.SetBuffer(Array.zeroCreate Packet.MaxSize, 0, Packet.MaxSize)
                    receivedState <- sender.ReceiveAsync(args)
                else
                    receivedState <- false
            else
                printf "SocketFailed: SocketReceivedPacketComplete\n"
            
        with
            | ex -> Console.WriteLine ex.Message

//    member private this.ProcessOutgoingPackets(packet : IPacketC2S, buffer : byte[], index : int) =
//        match EnumOfValue buffer.[index + 0] with
//        |   PacketC2SType.Core -> 
//                match EnumOfValue buffer.[index + 1] with
//                | PacketCoreType.Reliable -> 
//                    let reliable = (packet :?> Reliable)
//                    this.ProcessOutgoingPackets(reliable.Packet, buffer, index + Reliable.HeaderSize)
//                | PacketCoreType.ReliableAck -> ()
//                | _ -> ()
//        |   PacketC2SType.Chat -> 
//                Helper.UnHandledMsg(typeof<PacketC2SType>, buffer.[index], "OutgoingS2C")
//                let chatMsgOut = (packet :?> ChatMessageOut)
//                packet.Serialize(buffer, index) |> ignore
//                printf "msg: %s %s %d\n" (Enum.GetName(typeof<ChatType>, chatMsgOut.ChatType)) chatMsgOut.Message packet.Size
//        | _ -> Helper.UnHandledMsg(typeof<PacketC2SType>, buffer.[index], "OutgoingC2S")
        

    member private this.GenerateOutgoingPackets(buffer : byte[]) = 
        if (outGoingPacket0 <> null 
            || outGoingHighPriorityQueue.TryDequeue(&outGoingPacket0) 
            || outGoingNormalPriorityQueue.TryDequeue(&outGoingPacket0)) then

            outGoingPacketsCount <- 1
            outGoingPacket <- outGoingPacket0
            outGoingPacketSize <- outGoingPacket0.Size

            if (outGoingPacket :? Reliable) then
                let mutable reliablePacket = outGoingPacket :?> Reliable
                reliablePacket.Timestamp <- _linker.TickElapsed

            outGoingPacket0 <- null
            if (false && outGoingPacketSize <= 256) then
                outGoingPacketClusteredSize <- 2 + 1 + outGoingPacketSize
                clusterNotFull <- true
                while(clusterNotFull && (outGoingHighPriorityQueue.TryDequeue(&outGoingPacket0) || 
                                            outGoingNormalPriorityQueue.TryDequeue(&outGoingPacket0))) do
                        let outGoingPacket0Size = outGoingPacket0.Size
                        if (outGoingPacket0Size + 1 + outGoingPacketClusteredSize <= Packet.MaxSize) then
                            outGoingPacketsCount <- outGoingPacketsCount + 1
                            buffer.[outGoingPacketClusteredSize] <- (byte)outGoingPacket0Size
                            outGoingPacketClusteredSize <- outGoingPacketClusteredSize + 1
                            outGoingPacket0.Serialize(buffer, outGoingPacketClusteredSize) |> ignore
                            buffer.[outGoingPacketClusteredSize] <- outGoingPacket0.Type
                            //this.ProcessOutgoingPackets(outGoingPacket, buffer, outGoingPacketClusteredSize)
                            outGoingPacketClusteredSize <- outGoingPacketClusteredSize + outGoingPacket0Size
                            if (outGoingPacket0 :? Reliable) then
                                let mutable reliablePacket0 = outGoingPacket0 :?> Reliable
                                reliablePacket0.Timestamp <- _linker.TickElapsed
                            outGoingPacket0 <- null
                        else
                            clusterNotFull <- false
            if (outGoingPacketsCount > 1) then
                //printf "\nClustered: %d" outGoingPacketsCount
                buffer.[0] <- EnumToValue PacketCoreType.Core
                buffer.[1] <- EnumToValue PacketCoreType.Cluster
                buffer.[2] <- (byte)outGoingPacketSize
                outGoingPacket.Serialize(buffer, 3) |> ignore
                buffer.[3] <- outGoingPacket.Type
                outGoingPacketSize <- outGoingPacketClusteredSize
                //this.ProcessOutgoingPackets(outGoingPacket0, buffer, 3)
            else
                outGoingPacket.Serialize(buffer, 0) |> ignore
                buffer.[0] <- outGoingPacket.Type
                //this.ProcessOutgoingPackets(outGoingPacket0, buffer, 0)

        else
            outGoingPacketSize <- 0
        //printf "%s %s\n" (Helper.ByteToHex buffer.[0]) (Helper.ByteToHex buffer.[1])
        //                if (buffer.[0] = 0uy && buffer.[1] = (byte)PacketCoreType.ReliableAck) then
//                    let ack = new ReliableAck(buffer, 0, 0)
//                    printf "Non-Clustered ReliableACK - %d\n" ack.ReliableId


    member private this.ProcessIncomingPackets(buffer : byte[], index : int, size : int) =
        match EnumOfValue buffer.[index + 0] with
        |  PacketS2CType.Core -> 
            match EnumOfValue buffer.[index + 1] with
            | PacketCoreType.Cluster -> this.ClusterHandler(buffer, index, size)
            | PacketCoreType.Reliable -> this.ReliableHandler(buffer, index, size)
            | PacketCoreType.ReliableAck -> this.ReliableAckHandler(buffer, index, size)
            | PacketCoreType.SyncRequest -> this.SyncRequestHandler(buffer, index, size)
            | PacketCoreType.SyncResponse-> this.SyncResponseHandler(buffer, index, size)
            | PacketCoreType.SmallChunkBody -> this.SmallChunkBodyHandler(buffer, index, size)
            | PacketCoreType.SmallChunkTail -> this.SmallChunkTailHandler(buffer, index, size)
            | PacketCoreType.BigChunkBody -> this.BigChunkBodyHandler(buffer, index, size)
            | PacketCoreType.EncryptionRequest -> this.EncryptionRequestHandler(buffer, index, size)
            | PacketCoreType.EncryptionResponse -> this.EncryptionResponseHandler(buffer, index, size)
            | PacketCoreType.Disconnect-> this.DisconnectHandler(buffer, index, size)
            | _ -> this.UnknownCorePacketHandler(buffer, index, size)
        | _ -> this.PacketS2CHandler(buffer, index, size)
        ()

    member private this.ClusterHandler(buffer : byte[], index : int, size : int) = 
        //Console.WriteLine "ClusterHandler"
        let mutable clusterIndex = 2
        while (clusterIndex < size) do
            let packetSize = (int32)buffer.[clusterIndex]
            clusterIndex <- clusterIndex + 1
            this.ProcessIncomingPackets(buffer, clusterIndex, packetSize)
            clusterIndex <- clusterIndex + packetSize
        ()

    member private this.ReliableHandler(buffer : byte[], index : int, size : int) =  
        let reliableAck = new ReliableAck(buffer, index, size)
        this.SendPacketHighPriority(reliableAck)
        //printf "ReliableHandler %d\n" reliableAck.ReliableId
        //printf "reliablePacketsReceivedId:  GOT: \n"// reliableAck.ReliableId
        if ( reliablePacketsReceivedId = reliableAck.ReliableId) then
            reliablePacketsReceivedId <- reliablePacketsReceivedId + 1
            this.ProcessIncomingPackets(buffer, index + Reliable.HeaderSize, size - Reliable.HeaderSize)           
            let mutable exists = reliablePacketsRecieved_InTransit.TryRemove(reliablePacketsReceivedId, &packetS2CReader)
            while (exists) do
                //printf "RELIABLE PROCESS %d %d\n" reliableAck.ReliableId reliablePacketsReceivedId
                reliablePacketsReceivedId <- reliablePacketsReceivedId + 1
                this.ProcessIncomingPackets(packetS2CReader.Buffer, packetS2CReader.Index, packetS2CReader.Size)
                exists <- reliablePacketsRecieved_InTransit.TryRemove(reliablePacketsReceivedId, &packetS2CReader)
        else if (reliableAck.ReliableId > reliablePacketsReceivedId) then
            reliablePacketsRecieved_InTransit.TryAdd(reliableAck.ReliableId, new PacketS2CReader(buffer, index + Reliable.HeaderSize, size - Reliable.HeaderSize)) |> ignore//buffer.[index + Reliable.HeaderSize .. size - 1]) |> ignore
//        else
//            printf "GOT IT ALREADY RELIABLE %d %d\n" reliableAck.ReliableId reliablePacketsReceivedId
        ()

    member private this.ReliableAckHandler(buffer : byte[], index : int, size : int) = 
        let reliableAck = new ReliableAck(buffer, index, size)
        //printf "ReliableAckHandler %d\n" reliableAck.ReliableId
        let  exists, packet = reliablePacketsSent_InTransit.TryRemove(reliableAck.ReliableId)
        if (not exists) then
            printf "ReliableAckId %d not found. ID at: %d\n" reliableAck.ReliableId reliablePacketsSentId
        ()

    member private this.EncryptionRequestHandler(buffer : byte[], index : int, size : int) =
        //Console.WriteLine "EncryptionRequestHandler"
        this.SendPacketHighPriority(new Disconnect()) // this is not a server.
        ()

    member private this.EncryptionResponseHandler(buffer : byte[], index : int, size : int) = 
        //Console.WriteLine "EncryptionResponseHandler"
        let encryptionResponse = new EncryptionResponse(buffer, index, size)
        if (encryptionMode = EncryptionMode.Off) then
            this.SendPacketHighPriority(new EncryptionResponse(encryptionResponse.ServerKey))
        else
            packetEncryption.ServerKey <- encryptionResponse.ServerKey
        this.Connected(true)

    member private this.SyncRequestHandler(buffer : byte[], index : int, size : int) = 
        let syncRequest = new SyncRequest(buffer, index, size)
        this.SendPacketHighPriority(new SyncResponse(syncRequest.Timestamp, (_linker.TickElapsed/10)))
        //printf "SyncRequestHandler %d/%d %d/%d \n" totalPacketsSent syncRequest.TotalPacketsSent totalPacketsReceived syncRequest.TotalPacketsReceived
        ()

    member private this.SyncResponseHandler(buffer : byte[], index : int, size : int) = 
        //Console.WriteLine "SyncResponseHandler"
        let syncResponse = new SyncResponse(buffer, index, size)

        let clientTimeNow = (_linker.TickElapsed/10)
        
        let syncRoundTripTime = clientTimeNow - syncResponse.ReceivedTimestamp
        pingSumOfRoundTripTime <- pingSumOfRoundTripTime + syncRoundTripTime
        pingRoundTripCount <- pingRoundTripCount + 1
        pingAverage <- pingSumOfRoundTripTime / pingRoundTripCount

        //ping <- (uint32)((float)syncRoundTripTime * 3.0 / 5.0)
        ping <- syncRoundTripTime

        if (syncRoundTripTime > pingHigh) then
            pingHigh <- syncRoundTripTime

        if (pingLow = 0 || syncRoundTripTime < pingLow) then
            pingLow <- syncRoundTripTime

//        let lastSyncResponseTimeDiff = (clientTimeNow - lastSyncResponseReceived)
//        if (syncRoundTripTime >= lastSyncRoundTripTime  && lastSyncResponseTimeDiff <= 2200u) then
//            ()
//        else if (syncRoundTripTime >= lastSyncRoundTripTime * 2u && lastSyncResponseTimeDiff <= 8000u) then
//            ()
//        else
//            lastSyncResponseReceived <- clientTimeNow
//            lastSyncRoundTripTime <-  syncRoundTripTime
        //syncTimeDiff <- (int32)((int64)((double)syncRoundTripTime * 3.0 / 5.0) + ((int64)syncResponse.LocalTimestamp - (int64)clientTimeNow))
        //printf "SyncRes: %d %d %d %d %d\n" (this :> ISynchronize).ServerTime syncResponse.ReceivedTimestamp syncResponse.LocalTimestamp  syncTimeDiff TimeNow 
        //printf "PING current:%d ave:%d high:%d low:%d\n"  ping pingAverage pingHigh pingLow
        ()

    member private this.SmallChunkBodyHandler(buffer : byte[], index : int, size : int) = 
        Console.WriteLine "SmallChunkBodyHandler" 
        smallChunkManager.AddBody(buffer, index, size)
        ()

    member private this.SmallChunkTailHandler(buffer : byte[], index : int, size : int) =
        Console.WriteLine "SmallChunkTailHandler" 
        let smallChunkBuffer = smallChunkManager.AddTail(buffer, index, size)
        this.ProcessIncomingPackets(smallChunkBuffer, 0 , smallChunkBuffer.Length)
        ()

    member private this.BigChunkBodyHandler(buffer : byte[], index : int, size : int) = 
        Console.WriteLine "BigChunkBodyHandler"
        if (bigChunkManager.AddBody(buffer, index, size)) then
            let bigChunkBuffer = bigChunkManager.EmptyBuffer()
            this.ProcessIncomingPackets(bigChunkBuffer, 0 , bigChunkBuffer.Length)
        ()

    member private this.UnknownCorePacketHandler(buffer : byte[], index : int, size : int) =
        //ToDo Error:
        Helper.UnHandledMsg(typeof<PacketCoreType>, buffer.[index], "UnknownCorePacketHandler")
        printf "%s\n" (Helper.ByteToHex buffer.[index + 1])
        ()

    member private this.DisconnectHandler(buffer : byte[], index : int, size : int) =
        this.Disconnected(true)
                                        
    member private this.PacketS2CHandler(buffer : byte[], index : int, size : int) =
        let packet = new PacketS2CReader(buffer, index, size)
        let exists, rpEvent = receivedPacketEventMap.TryGetValue(packet.S2CType)
        if (exists) then
            _linker.QueueTask(new ReceivedPacketTask(rpEvent, this, packet))
        //else
            //Helper.UnHandledMsg(typeof<PacketS2CType>, buffer.[index], "PacketS2CHandler")

    member private this.Reset() =
        smallChunkManager.Reset()
        bigChunkManager.Reset()
        packetEncryption.Reset()

        outGoingPacket0 <- null
        outGoingPacket <- null
        outGoingPacketSize <- 0
        outGoingPacketClusteredSize <- 0
        outGoingPacketsCount <- 0
        let mutable packetDump : IPacketC2S = null
        while (outGoingNormalPriorityQueue.TryDequeue(&packetDump)) do ()
        outGoingQueueEvent.Reset() |> ignore
        while (outGoingHighPriorityQueue.TryDequeue(&packetDump)) do ()
        outGoingQueueEvent.Reset() |> ignore

        reliablePacketsSentId <- -1
        reliablePacketsReceivedId <- 0
        reliablePacketsSent_InTransit.Clear()
        reliablePacketsRecieved_InTransit.Clear()

        totalPacketsSent <- 0u
        totalPacketsReceived <- 0u
        lastPacketSentTime <- 0
        lastPacketReceivedTime <- 0

        lastSyncRequestSent <- 0
        lastSyncResponseReceived <- 0
        lastSyncRoundTripTime <- 0
   
        eXEChecksum <- 0u
        settingChecksum <- 0u
        mapChecksum <- 0u
        weaponCount <- 0u
        slowFPS <- false

        ping <- 0
        pingAverage <- 10
        pingLow <- 0
        pingHigh <- 0
 
 and internal ConnectedTask =
     struct
        val ConnectedEvent : Event<ZoneConnectedHandle,bool>
        val Sender : Object
        val CanConnect : bool
        new(connectedEvent, sender, canConnect) =
            {
                ConnectedEvent = connectedEvent;
                Sender = sender;
                CanConnect = canConnect;
            }
        interface IFcEngineTask with
            member this.Do() =
                this.ConnectedEvent.Trigger(this.Sender, this.CanConnect)
                ()
    end
and internal DisconnectedTask =
    struct
        val DisconnectedEvent : Event<ZoneDisconnectedHandle,bool>
        val Sender : Object
        val LostConnection : bool
        new(disconnectedEvent, sender, lostConnection) =
            {
                DisconnectedEvent = disconnectedEvent;
                Sender = sender;
                LostConnection = lostConnection;
            }
        interface IFcEngineTask with
            member this.Do() =
                this.DisconnectedEvent.Trigger(this.Sender, this.LostConnection)
                ()
    end
 and internal ReceivedPacketTask =
    struct
        val ReceivedPacketEvent : Event<ReceivedPacketHandle,PacketS2CReader>
        val Sender : Object
        val Packet : PacketS2CReader
        new(receivedPacketEvent, sender, packet) =
            {
                ReceivedPacketEvent = receivedPacketEvent;
                Sender = sender;
                Packet = packet;
            }
        interface IFcEngineTask with
            member this.Do() =
                this.ReceivedPacketEvent.Trigger(this.Sender, this.Packet)
                ()
    end
