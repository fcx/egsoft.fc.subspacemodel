﻿// Copyright (c) 2012 Fc <fcx.code@gmail.com>
//            All Rights Reserved
//
// This software is provided 'as-is', without any express or implied 
// warranty. In no event will the author be held liable for any damages 
// arising from the use of this software.
//
namespace EGSoft.Fc.SubspaceModel.Net.Security

type EncryptionMode =
    | On = 0x00uy
    | Off = 0x01uy

[<AllowNullLiteral>]
type Encryption() =
    let mutable validServerKey = false    
    
    let mutable clientKey = (uint32)((new System.Random()).Next(1, System.Int32.MaxValue) * -1)
    let mutable serverKey = 0u
    
    let mutable serverKeyTable : byte[] = Array.zeroCreate 4
    let mutable cryptTable : byte[] = Array.zeroCreate 520
    
    member this.Reset() =
        validServerKey <- false
        clientKey <- (uint32)((new System.Random()).Next(1, System.Int32.MaxValue) * -1)
        serverKey <- 0u
        serverKeyTable <- Array.zeroCreate 4
        cryptTable <- Array.zeroCreate 520

    member this.ClientKey 
        with get() = clientKey

    member this.ServerKey
        with get() = 
            serverKey
        and set(_serverKey : uint32) =
            if (_serverKey <> 0u) then
                serverKey <- _serverKey

                for i in 0 .. 3 do
                    serverKeyTable.[i] <- (byte)(serverKey >>> (i * 8))

                let mutable oldSeed = 0UL
                let mutable tempSeed = (uint64)serverKey &&& 0x00000000FFFFFFFFUL
                for i in 0 .. (520/2 - 1) do
                    oldSeed <- tempSeed
                    tempSeed <- ((oldSeed * 0x834E0B5FUL) >>> 48) &&& 0xFFFFFFFFUL;
                    tempSeed <- (tempSeed + (tempSeed >>> 31)) &&& 0xFFFFFFFFUL;
                    tempSeed <- (((oldSeed % 0x1F31DUL) * 0x41A7UL) - (tempSeed * 0xB14UL) + 0x7BUL) &&& 0xFFFFFFFFUL
                    if (tempSeed > 0x7FFFFFFFUL) then
                        tempSeed <- (tempSeed + 0x7FFFFFFFUL) &&& 0xFFFFFFFFUL
                    cryptTable.[i*2] <- (byte)tempSeed &&& 0xFFuy
                    cryptTable.[i*2 + 1] <- (byte)(tempSeed >>> 8) &&& 0xFFuy
                validServerKey <- true

    member this.EncryptData(data : byte[], size : int32) =
        let offset = if data.[0] = 0x00uy then 2 else 1
        if (validServerKey) then
            let outTempKey = Array.copy serverKeyTable
            for i in 0 .. size - (1 + offset) do
                let temp = data.[offset + i] ^^^ cryptTable.[i] ^^^ outTempKey.[i%4]
                outTempKey.[i%4] <- temp
                data.[offset + i] <- temp
            ()

    member this.DecryptData(data : byte[], size : int32) =
        let offset = if data.[0] = 0x00uy then 2 else 1
        if (validServerKey) then
            let inTempKey = Array.copy serverKeyTable
            for i in 0 .. size - (1 + offset) do
                let tempData = data.[offset + i]
                data.[offset + i] <- tempData ^^^ cryptTable.[i] ^^^ inTempKey.[i%4]
                inTempKey.[i%4] <- tempData
            ()