﻿// Copyright (c) 2012 Fc <fcx.code@gmail.com>
//            All Rights Reserved
//
// This software is provided 'as-is', without any express or implied 
// warranty. In no event will the author be held liable for any damages 
// arising from the use of this software.
//
namespace EGSoft.Fc.SubspaceModel.Modules

open System
open System.Collections.Generic
open System.Text.RegularExpressions

open Microsoft.FSharp.Core
open Microsoft.FSharp.Collections
open LanguagePrimitives

open EGSoft.Fc.EngineModel
open EGSoft.Fc.SubspaceModel
open EGSoft.Fc.SubspaceModel.Utilities
open EGSoft.Fc.SubspaceModel.Net
open EGSoft.Fc.SubspaceModel.Net.Utilities
open EGSoft.Fc.SubspaceModel.Net.Packets
open EGSoft.Fc.SubspaceModel.Net.Packets.Core
open EGSoft.Fc.SubspaceModel.Net.Packets.C2S
open EGSoft.Fc.SubspaceModel.Net.Packets.S2C
open EGSoft.Fc.SubspaceModel.Net.Security

type BroadcastMessageReceivedHandle = delegate of sender:obj * msg:String -> unit
type ServerMessageReceivedHandle = delegate of sender:obj * msg:String -> unit   
type MessageReceivedHandle = delegate of sender:obj * msgInfo:MessageInfo -> unit

module MessageChannel =
    let Warning = "Warning"
    let Chat = "Chat"
    let Public = "Public"
    let Private = "Private"
    let PrivateRemote = "Private Remote"
    let PublicMacro = "Public Macro"
    let Team = "Team"
    let TeamBroadcast = "TeamBroadcast"
    let Broadcast = "Broadcast"
    let Server = "Server"
    

[<AllowNullLiteral>]
type IMessenger =
    inherit IFcEngineModule
    abstract JoinChatChannel : chatName:string -> bool
    abstract LeaveChatChannel : chatName:string -> unit
    //abstract Reply : msgInfo:MessageInfo * msg:String * soundType:SoundType -> bool
    //abstract ReplyPrivately : msgInfo:MessageInfo * msg:String * soundType:SoundType -> bool
    abstract Send : msgInfo:MessageInfo * message:string * soundType:SoundType-> unit
    abstract Send : messageType:MessageType * toChatId:uint16 * toPlayerId:uint16 * toPlayerName:string * message:string * soundType:SoundType -> unit
    abstract Send : msg:string * soundType:SoundType -> MessageType
    abstract Send : toPlayerId:uint16 * msg:string * soundType:SoundType-> MessageType
    abstract SendZone : msg:string * SoundType-> unit
    abstract SendArena : msg:string * SoundType-> unit
    abstract SendWarning : toPlayerId:uint16 * msg:string * soundType:SoundType-> unit
    abstract SendPrivate : toPlayerName:String * msg:string * soundType:SoundType-> MessageType
    abstract SendPrivate : toPlayerId:uint16 * msg:string * soundType:SoundType-> unit
    abstract SendPrivateRemote : toPlayerName:String * message:string * soundType:SoundType-> unit
    abstract SendPublic : msg:string * soundType:SoundType-> unit
    abstract SendPublicMacro : msg:string * soundType:SoundType-> unit
    abstract SendChat : channel:string * msg:string * soundType:SoundType-> unit
    abstract SendChat : channelId:uint16 * msg:string * soundType:SoundType-> unit
    abstract SendTeam : msg:string * soundType:SoundType-> unit
    abstract SendTeamBroadcast : toPlayerId:uint16 * msg:string * soundType:SoundType-> unit
    abstract ChatChannelNameToIdMap : unit -> Map<string, int>
        with get
    [<CLIEvent>]
    abstract MessageReceived : IEvent<MessageReceivedHandle,MessageInfo>
//    [<CLIEvent>]
//    abstract BroadcastMessageChannel : IEvent<BroadcastMessageReceivedHandle,String>
//    [<CLIEvent>]
//    abstract ServerMessageChannel : IEvent<ServerMessageReceivedHandle,String>


//type MessageCommandArguments(input:String) =
//    let args = new Dictionary<String,String>(StringComparer.OrdinalIgnoreCase)
//
//    interface IFcCommandArguments with
//        member this.GetValue key = 
//            let mutable value = null
//            if (not (args.TryGetValue(key, &value))) then
//                raise (new Exception("Parameter name not found."))
//            else
//                value
//
//        member this.ContainsKey(key:String) = args.ContainsKey(key)
//
//        member this.Item
//                with get(i) = args.[i]
//        
//type MessageCommandContext(message:IMessage, messageInfo:MessageInfo, cmdArgs:IFcCommandArguments) =
//    let _messageInfo = messageInfo
//    let _message = message
//    let _cmdArgs = cmdArgs
//    interface IFcCommandContext with
//        member this.Channel = _messageInfo.MessageType.ToString()
//        member this.Username = _messageInfo.PlayerName
//        member this.Arguments = _cmdArgs
//        member this.Reply(msg:String) =
//            _message.Send(_messageInfo, msg, SoundType.None) |> ignore
//        member this.ReplyPrivately(msg:String) =
//            _message.SendPrivate(_messageInfo.PlayerName, msg, SoundType.None) |> ignore
//    

[<FcEngineModuleInfo(
    ModuleName = "Messenger",
    Author = "Fc",
    Version = "v1.0")>]
type Messenger() =
    let mutable _linker : IFcEngineLinker = null
    let mutable _zone : IZone = null
    let mutable _arena : IArena = null

    let messageRecievedEvent = new Event<MessageReceivedHandle, MessageInfo>()
    let broadcastMessageReceivedEvent = new Event<BroadcastMessageReceivedHandle, String>()
    let serverMessageReceivedEvent = new Event<ServerMessageReceivedHandle, String>()
    let mutable channelNameToIdMap : Map<string, int> = Map.empty

    interface IMessenger with
        member this.OnLoad(linker:IFcEngineLinker) =
            _linker <- linker
            _zone <- linker.AttachModule<IZone>()
            _arena <- linker.AttachModule<IArena>()
            _zone.RecievedPacketAddHandler((byte)(EnumToValue PacketS2CType.Chat), new ReceivedPacketHandle(this.MessageInHandler))
            true
        member this.OnUnload(linker:IFcEngineLinker) =
            true

        [<CLIEvent>]
        member this.MessageReceived = messageRecievedEvent.Publish
//        [<CLIEvent>]
//        member this.BroadcastMessageChannel = broadcastMessageReceivedEvent.Publish
//        [<CLIEvent>]
//        member this.ServerMessageChannel = serverMessageReceivedEvent.Publish

        member this.ChatChannelNameToIdMap = channelNameToIdMap

        member this.JoinChatChannel( chatName : string) =
            let mutable can = true
            let cnameArray = chatName.Trim(',').Split(',')
            for cname in cnameArray do
                let cindex = ref 0
                if (can && channelNameToIdMap.ContainsKey(cname) = false && channelNameToIdMap.Count + 1 < 10) then
                    let newChannelMap = 
                        channelNameToIdMap 
                        |> Map.add(cname) 0
                        |> Map.map (fun k v -> 
                                        incr cindex
                                        !cindex)
                    channelNameToIdMap <- newChannelMap
                else 
                    can <- false
            let chatList = Map.fold (fun state key value -> state + key + ",") "?chat=" channelNameToIdMap
            (this :> IMessenger).SendPublic(chatList, SoundType.None)
            can

        member this.LeaveChatChannel( chatName : string) =
            let cnameArray = chatName.Trim(',').Split(',')
            for cname in cnameArray do
                let cindex = ref 0
                if (channelNameToIdMap.ContainsKey(cname) = true) then
                    let newChannelMap = 
                        channelNameToIdMap 
                        |> Map.add(cname) 0
                        |> Map.map (fun k v -> 
                                        incr cindex
                                        !cindex)
                    channelNameToIdMap <- newChannelMap
            let chatList = Map.fold (fun state key value -> state + key + ",") "?chat=" channelNameToIdMap
            (this :> IMessenger).SendPublic(chatList, SoundType.None)
        
        member this.Send(msgInfo:MessageInfo, message : String, soundType : SoundType) =
            (this :> IMessenger).Send(msgInfo.MessageType, msgInfo.ChatId, msgInfo.PlayerId, msgInfo.PlayerName, message, soundType)

        member this.Send(messageType : MessageType, toChatId:uint16, toPlayerId:uint16, toPlayerName:String, message : String, soundType : SoundType) =
            match messageType with
            | MessageType.Warning ->
                (this :> IMessenger).SendWarning(toPlayerId, message, soundType)
            | MessageType.Chat ->
                (this :> IMessenger).SendChat(toChatId, message, soundType)
            | MessageType.PrivateRemote ->
                (this :> IMessenger).SendPrivateRemote(toPlayerName, message, soundType)
            | MessageType.Private ->
                (this :> IMessenger).SendPrivate(toPlayerId, message, soundType)
            | MessageType.Public ->
                (this :> IMessenger).SendPublic(message, soundType)
            | MessageType.TeamBroadcast ->
                (this :> IMessenger).SendTeamBroadcast(toPlayerId, message, soundType)
            | MessageType.Team ->
                (this :> IMessenger).SendTeam(message, soundType)
            | MessageType.PublicMacro ->
                (this :> IMessenger).SendPublicMacro(message, soundType)
            | MessageType.Broadcast -> () //Warning
            | MessageType.Server -> () //Warning
            |_ -> () //Error
                
            

        member this.Send(message : String, soundType : SoundType) = 
            (this :> IMessenger).Send(0us, message, soundType)

        member this.Send(playerId : uint16, message : String, soundType : SoundType) =
            if message.Length > 0 then
                match message.[0] with
                | ':' ->  
                    let privMsg = Regex.Match(message, @":(.*):(.*)")
                    if (message.LastIndexOf(':') <> 0) then
                        (this :> IMessenger).SendPrivate(privMsg.Groups.[1].Value, privMsg.Groups.[2].Value, soundType)
                    else
                        (this :> IMessenger).SendPublic(message, soundType)
                        MessageType.Public
                | '/' ->
                    if (message.Length > 1 && message.[1] = '/') then
                        let msg = if (message.Length > 2) then message.Substring(2) else String.Empty
                        (this :> IMessenger).SendTeam(msg, soundType)
                        MessageType.Team
                    else
                        let msg = if (message.Length > 1) then message.Substring(1) else String.Empty
                        (this :> IMessenger).SendPrivate((if playerId = 0us then _arena.UserTargetingPlayerId else playerId), msg, soundType)
                        MessageType.Private
                | ''' -> 
                    let msg = if (message.Length > 1) then message.Substring(1) else String.Empty
                    (this :> IMessenger).SendTeam(msg, soundType)
                    MessageType.Team
                | '"' -> 
                    let msg = if (message.Length > 1) then message.Substring(1) else String.Empty
                    (this :> IMessenger).SendTeamBroadcast(playerId, msg, soundType)
                    MessageType.TeamBroadcast
                | ';' -> 
                    let regMsg = Regex.Match(message, @";([0-9]+);(.*)")
                    if (regMsg.Success) then
                        this.Send(MessageType.Chat, 0us, ";" + regMsg.Groups.[1].Value + ";", regMsg.Groups.[2].Value, soundType)
                        MessageType.Chat
                    else
                        let msg = if (message.Length > 1) then message.Substring(1) else String.Empty
                        this.Send(MessageType.Chat, 0us, ";1;", msg, soundType) 
                        MessageType.Chat     
                | '*' -> 
                    if (message.IndexOf("*warn ") = 0 && message.Length > 6) then
                        (this :> IMessenger).SendWarning(playerId, message.Substring(6), soundType)
                        MessageType.Warning
                    else if (message.IndexOf("*arena ") = 0 && message.Length > 7) then
                        (this :> IMessenger).SendArena(message.Substring(7), soundType)
                        MessageType.Broadcast
                    else if (message.IndexOf("*zone ") = 0 && message.Length > 6) then
                        (this :> IMessenger).SendZone(message.Substring(6), soundType)
                        MessageType.Broadcast
                    else
                        this.Send(MessageType.Public, playerId, String.Empty, message, soundType)
                        MessageType.Public
                | _ -> 
                    (this :> IMessenger).SendPublic(message, soundType)
                    MessageType.Public
            else
                MessageType.Public
               

        member this.SendPublic(message : String, soundType : SoundType) =
            this.Send(MessageType.Public, 0us, String.Empty, message, soundType)

        member this.SendPublicMacro(message : String, soundType : SoundType) = 
            this.Send(MessageType.PublicMacro, 0us, String.Empty, message, soundType)

        member this.SendPrivate(playerId : uint16, message : String, soundType : SoundType) =
            this.Send(MessageType.Private, playerId, String.Empty, message, soundType)

        member this.SendPrivate(toPlayerName:String, message:String, soundType:SoundType) =
            let mutable playerInfo = null
            if (_arena.PlayerInfoMap.TryGetPlayer(toPlayerName, &playerInfo)) then
                this.Send(MessageType.Private, playerInfo.PlayerId, String.Empty, message, soundType)
                MessageType.Private
            else
                (this :> IMessenger).SendPrivateRemote(toPlayerName, message, soundType)
                MessageType.PrivateRemote

        member this.SendPrivateRemote(toPlayerName:String, message : String, soundType : SoundType) =
            this.Send(MessageType.PrivateRemote, 0us, (":" + toPlayerName + ":"), message, soundType)

        member this.SendTeam(message : String, soundType : SoundType) =
            this.Send(MessageType.Team, 0us, String.Empty, message, soundType)

        member this.SendTeamBroadcast(playerId : uint16, message : String, soundType : SoundType) =
            this.Send(MessageType.TeamBroadcast, playerId, String.Empty, message, soundType)

        member this.SendChat(channel : string, message : String, soundType : SoundType) =
            let channelName = channelNameToIdMap.TryFind(channel)
            if (channelName.IsSome) then
                this.Send(MessageType.Chat, 0us, ";" + channelName.Value.ToString() + ";", message, soundType)
            else
                let mutable channelId = 0us
                if (UInt16.TryParse(channel, &channelId)) then
                    (this :> IMessenger).SendChat(channelId, message, soundType)

        member this.SendChat(channelId : uint16, message : String, soundType : SoundType) =
                this.Send(MessageType.Chat, 0us, ";" + channelId.ToString() + ";", message, soundType)

        member this.SendZone(message : String, soundType : SoundType) =
            this.Send(MessageType.Public, 0us, "*zone ", message, soundType) 

        member this.SendArena(message : String, soundType : SoundType) = 
            this.Send(MessageType.Public, 0us, "*arena ", message, soundType) 

        member this.SendWarning(playerId : uint16, message : String, soundType : SoundType) = 
            this.Send(MessageType.Private, 0us, "*warn ", message, soundType) 

    member private this.Send(messageType:MessageType, playerId : uint16, prefix : string, message : string, soundType : SoundType) =
        let msg = 
            if (String.IsNullOrEmpty(prefix)) then
                message
            else
                prefix + message

        if (msg.Length > MessageOut.MaxSize) then
            let chatMessage = new MessageOut(messageType, soundType, playerId, msg.Substring(0, MessageOut.MaxSize))
            _zone.SendPacket(chatMessage, true)
            let msgRemaining = msg.Substring(MessageOut.MaxSize, msg.Length - MessageOut.MaxSize)
            this.Send(messageType, playerId, prefix, msgRemaining, SoundType.None)
        else
            let chatMessage = new MessageOut(messageType, soundType, playerId, msg)
            _zone.SendPacket(chatMessage, true)

    member private this.MessageInHandler sender packet =
        let msg = new MessageIn(packet)
        let playerOrChannelId, playerName, playerMessage =
            match msg.MessageType with
            | MessageType.Broadcast ->
                0us, String.Empty, msg.Message
            | MessageType.Server ->
                0us, String.Empty, msg.Message
            | MessageType.Warning ->
                let privMsg = Regex.Match(msg.Message, @"MODERATOR WARNING:\s(.*)\s-(.*)", RegexOptions.RightToLeft)
                if (privMsg.Success) then
                    msg.PlayerId, privMsg.Groups.[2].Value, privMsg.Groups.[1].Value
                else
                    0us, String.Empty, msg.Message
                    //raise (new Exception("Unhandled ChatMessageIn:Channel syntax"))
            | MessageType.Chat ->
                let privMsg = Regex.Match(msg.Message, @"([0-9]+):(.*)> (.*)")
                if (privMsg.Success) then
                    UInt16.Parse(privMsg.Groups.[1].Value), privMsg.Groups.[2].Value, privMsg.Groups.[3].Value
                else
                    raise (new Exception("Unhandled ChatMessageIn:Channel syntax"))
            | MessageType.PrivateRemote ->
                let privMsg = Regex.Match(msg.Message, @"\((.*)\)>(.*)")
                if (privMsg.Success) then
                    msg.PlayerId, privMsg.Groups.[1].Value, privMsg.Groups.[2].Value
                else
                    let specialPrivMsg = Regex.Match(msg.Message, @".*:\s\((.*)\)\s\(.*\):\s(.*)")
                    if (specialPrivMsg.Success) then
                        msg.PlayerId, specialPrivMsg.Groups.[1].Value, specialPrivMsg.Groups.[2].Value
                    else
                        raise (new Exception("Unhandled ChatMessageIn:PrivateRemoteMessage syntax"))
                    //msg.PlayerId, MessageType.PrivateRemoteMessage.ToString(), msg.Message
            | MessageType.Private ->
                let exists, playerInfo = _arena.PlayerInfoMap.TryGetPlayer(msg.PlayerId)
                playerInfo.PlayerId, playerInfo.Name, msg.Message
            | MessageType.Public ->
                let exists, playerInfo = _arena.PlayerInfoMap.TryGetPlayer(msg.PlayerId)
                playerInfo.PlayerId, playerInfo.Name, msg.Message
            | MessageType.PublicMacro ->
                let exists, playerInfo = _arena.PlayerInfoMap.TryGetPlayer(msg.PlayerId)
                playerInfo.PlayerId, playerInfo.Name, msg.Message
            | MessageType.TeamBroadcast ->
                let exists, playerInfo = _arena.PlayerInfoMap.TryGetPlayer(msg.PlayerId)
                playerInfo.PlayerId, playerInfo.Name, msg.Message
            | MessageType.Team ->
                let exists, playerInfo = _arena.PlayerInfoMap.TryGetPlayer(msg.PlayerId)
                playerInfo.PlayerId, playerInfo.Name, msg.Message
            | _ -> 
                raise (new Exception("Unknown MessageType"))

        let msgInfo = new MessageInfo(msg.MessageType, msg.SoundType, playerOrChannelId, playerName, playerMessage)

//        if (playerMessage.Length > 0 && playerMessage.[0] = '!') then
//            let firstSpace = playerMessage.IndexOf(' ')
//
//            let cmdStr, cmdArgsStr = 
//                if (firstSpace > 0) then
//                    playerMessage.Substring(1, firstSpace), playerMessage.Substring(firstSpace)
//                else if (playerMessage.Length <> 1) then
//                    playerMessage.Substring(1), String.Empty
//                else
//                    String.Empty, String.Empty
//            
//            if (not (String.IsNullOrEmpty(cmdStr))) then
//                let moduleNameCmdNameStr = cmdStr.Split('.')
//                let moduleNameStr, cmdNameStr =
//                    if (moduleNameCmdNameStr.Length = 2) then
//                        moduleNameCmdNameStr.[0], moduleNameCmdNameStr.[1]
//                    else
//                        String.Empty, moduleNameCmdNameStr.[0]
//                let mutable command = null
//                if (_linker.FindCommand(moduleNameStr, cmdNameStr, &command)) then
//                    let channelType = msg.MessageType.ToString()
//                    if (command.ContainsChannel(channelType)) then
//                        let cmdArgs = new MessageCommandArguments(cmdArgsStr)
//                        let context = new MessageCommandContext(this, msgInfo, cmdArgs)
//                        command.Execute(context)
//                    else
//                        ()
//                    ()
//                else
//                    if (msg.MessageType = MessageType.Private) then
//                        (this :> IMessage).SendPrivate(playerOrChannelId, "Unknown command. Try !help", SoundType.None)
//                    else if (msg.MessageType = MessageType.PrivateRemote) then
//                        (this :> IMessage).SendPrivateRemote(playerName, "Unknown command. Try !help", SoundType.None)
//            else
//                if (msg.MessageType = MessageType.Private) then
//                    (this :> IMessage).SendPrivate(playerOrChannelId, "Unknown command. Try !help", SoundType.None)
//                else if (msg.MessageType = MessageType.PrivateRemote) then
//                    (this :> IMessage).SendPrivateRemote(playerName, "Unknown command. Try !help", SoundType.None)
        
        
        messageRecievedEvent.Trigger(this, msgInfo)



//        member this.SendRemotePrivate(message : String, soundType : SoundType) =
//            let privMsg = Regex.Match(message, @":(.*):(.*)")
//            if (privMsg.Success) then
//                let playerName = privMsg.Groups.[1].Value
//                let mutable playerInfo = null
//                if (_arena.PlayerInfoMap.TryGetPlayer(playerName, &playerInfo)) then
//                    (this :> IChat).Send(MessageType.PrivateMessage, playerInfo.PlayerId, String.Empty, privMsg.Groups.[2].Value, soundType)
//                else
//                    (this :> IChat).Send(MessageType.PrivateRemoteMessage, 0us, ":" + playerName + ":", privMsg.Groups.[2].Value, soundType)


//        member this.Reply(msgInfo:MessageInfo, message:String, soundType:SoundType) =
//            match msgInfo.MessageType with
//            | MessageType.Warning ->
//                (this :> IMessage).SendWarning(msgInfo.PlayerId, message, soundType)
//                true
//            | MessageType.Chat ->
//                this.Send(MessageType.Chat, 0us, ";" + (msgInfo.ChatId.ToString()) + ";", message, soundType)
//                true
//            | MessageType.PrivateRemote ->
//                (this :> IMessage).SendPrivate(msgInfo.PlayerName, message, soundType) |> ignore
//                true
//            | MessageType.Private ->
//                (this :> IMessage).SendPrivate(msgInfo.PlayerName, message, soundType) |> ignore
//                true
//            | MessageType.TeamBroadcast ->
//                (this :> IMessage).SendTeamBroadcast(msgInfo.PlayerId, message, soundType)
//                true
//            | MessageType.Team ->
//                (this :> IMessage).SendTeam(message, soundType)
//                true
//            | MessageType.PublicMacro ->
//                (this :> IMessage).SendPublicMacro(message, soundType)
//                true
//            | MessageType.Public ->
//                (this :> IMessage).SendPublic(message, soundType)
//                true
//            | _ -> false
//
//        member this.ReplyPrivately(msgInfo:MessageInfo, message:String, soundType:SoundType) =
//            match msgInfo.MessageType with
//            | MessageType.Warning
//            | MessageType.Chat
//            | MessageType.PrivateRemote 
//            | MessageType.Private
//            | MessageType.TeamBroadcast
//            | MessageType.Team
//            | MessageType.PublicMacro
//            | MessageType.Public ->
//                (this :> IMessage).SendPrivate(msgInfo.PlayerName, message, soundType) |> ignore
//                true
//            | _ -> false