﻿namespace EGSoft.Fc.SubspaceModel.Modules
//
//open System
//open System.Text
//open System.Threading
//open System.Collections.Concurrent
//open System.Collections.Generic
//open System.Runtime.InteropServices
//open System.Text.RegularExpressions
//
//open LanguagePrimitives
//
//open EGSoft.Fc.EngineModel
//open EGSoft.Fc.EngineModel.Modules
//open EGSoft.Fc.SubspaceModel
//
//type CommandArguments(args:Dictionary<String,String>) =
//    let args = new Dictionary<String,String>(args)
//
//    interface IFcCommandArguments with
//        member this.GetValue key = 
//            let mutable value = null
//            if (not (args.TryGetValue(key, &value))) then
//                raise (new Exception("Parameter name not found."))
//            else
//                value
//
//        member this.TryGetOptionalValue(key:String, [<Out>] value:byref<String>) =
//            let mutable tempValue = String.Empty
//            if (args.TryGetValue(key, &tempValue`)) then
//                value <- tempValue
//                true
//            else
//                false
//
//        member this.ContainsKey(key:String) = args.ContainsKey(key)
//
//        member this.Item
//                with get(i) = args.[i]
//        
//type CommandContext(message:IMessage, messageInfo:MessageInfo, cmdArgs:IFcCommandArguments) =
//    let _messageInfo = messageInfo
//    let _message = message
//    let _cmdArgs = cmdArgs
//    interface IFcCommandContext with
//        member this.Channel = _messageInfo.MessageType.ToString()
//        member this.Username = _messageInfo.PlayerName
//        member this.Arguments = _cmdArgs
//        member this.Reply(msg:String) =
//            _message.Send(_messageInfo, msg, SoundType.None) |> ignore
//        member this.ReplyPrivately(msg:String) =
//            _message.SendPrivate(_messageInfo.PlayerName, msg, SoundType.None) |> ignore
//
//
//[<AllowNullLiteral>]
//type ICommand =
//    inherit IFcEngineModule
//
//3[<AllowNullLiteral>]
//[<FcEngineModuleInfo(
//    ModuleName = "Command",
//    Author = "Fc",
//    Version = "v1.0")>]
//type Command() =
//    let mutable _linker = null
//    let mutable _info : IFcEngineModuleEngineInfo = null
//
//    let mutable _core = null
//    let mutable _coreManager : IFcCoreManager = null
//
//    let mutable _message : IMessage = null
//
//    interface ICommand with
//        member this.OnLoad(linker:IFcEngineLinker) =
//            _linker <- linker
//            _info <- info
//
//            _core <- _linker.AttachModule<IFcCore>()
//            _coreManager <- _core.GetCoreManager(info)
//
//            _core.HelpCommandExecuted.AddHandler(new FcCommandExecutedHandle(this.HelpCommandExecutedHandler))
//            _core.AboutCommandExecuted.AddHandler(new FcCommandExecutedHandle(this.AboutCommandExecutedHandler))
//
//            _message <- _linker.AttachModule<IMessage>()
//            _message.MessageChannel.AddHandler(new MessageReceivedHandle(this.MessageReceivedHandler))
//            true
//
//        member this.OnUnload(linker:IFcEngineLinker) =
//            true
//
//    member this.MessageReceivedHandler sender (msgInfo:MessageInfo) =
//        let msg = msgInfo.Message
//        if (msg.Length > 0 && msg.[0] = '!') then
//            let firstSpace = msg.IndexOf(' ')
//            ()
//            let cmdStr, cmdArgsStr = 
//                if (firstSpace > 0) then
//                    msg.Substring(1, firstSpace - 1), msg.Substring(firstSpace + 1)
//                else if (msg.Length <> 1) then
//                    msg.Substring(1), String.Empty
//                else
//                    String.Empty, String.Empty
//            
//            if (not (String.IsNullOrEmpty(cmdStr))) then
//                let moduleNameCmdNameStr = cmdStr.Split('.')
//                let moduleNameStr, cmdNameStr =
//                    if (moduleNameCmdNameStr.Length = 2) then
//                        moduleNameCmdNameStr.[0], moduleNameCmdNameStr.[1]
//                    else
//                        "FcCore", moduleNameCmdNameStr.[0]
//
//                let mutable command = null
//                if (_coreManager.TryGetCommand(moduleNameStr, cmdNameStr, &command)) then
//                    let channelType = msgInfo.MessageType.ToString()
//                    if (command.ContainsChannel(channelType)) then
//                        let mutable isSuccess = true
//                        let args = new Dictionary<String,String>(StringComparer.OrdinalIgnoreCase)
//                        
//                        let paramKeyValueRegex = new Regex(@"[\S]+:[\w\s]+\b(?!:)")
//
//                        for m in paramKeyValueRegex.Matches(cmdArgsStr) do
//                            let keyValuePair = m.Value.Split(':')
//                            if (keyValuePair.Length = 2) then
//                                if (not (args.ContainsKey(keyValuePair.[0]))) then
//                                    args.Add(keyValuePair.[0], keyValuePair.[1])
//                            else
//                                isSuccess <- false
//                                let errorz = "Error: Incorrect syntax. Usage: !moduleName|Alias.cmdName|Alias param1Name|Alias:value1 param2Name|Alias:value2 ..."
//                                if (msgInfo.MessageType = MessageType.Private) then
//                                    _message.SendPrivate(msgInfo.PlayerId, errorz, SoundType.None)
//                                else if (msgInfo.MessageType = MessageType.PrivateRemote) then
//                                    _message.SendPrivate(msgInfo.PlayerId, errorz, SoundType.None)
//                                
//                        if (isSuccess) then
//                            if (args.Count = 0 && (not (String.IsNullOrEmpty(cmdArgsStr)))) then
//                                if (command.Parameters.Count = 1) then
//                                    let enumerator = command.Parameters.GetEnumerator()
//                                    if (enumerator.MoveNext()) then
//                                        let onlyParam = enumerator.Current
//                                        args.Add(onlyParam.Name, cmdArgsStr)
//
//                            let mutable errors = String.Empty
//                            for p in command.Parameters do
//                                let mutable value = String.Empty
//                                if (args.TryGetValue(p.Name, &value)) then
//                                    args.Add(p.Alias, value)
//                                else
//                                    if (args.TryGetValue(p.Alias, &value)) then
//                                        args.Add(p.Name, value)
//                                    else
//                                        if ((not p.Optional)) then
//                                                errors <- errors + p.Name + "|" + p.Alias + ":" + p.Description + ", " 
//
//                            if (String.IsNullOrEmpty(errors)) then
//                                let context = new CommandContext(_message, msgInfo, new CommandArguments(args))
//                                command.Execute(context)
//                            else
//                                errors <- "Error: Missing parameter(s) - " + errors
//                                if (msgInfo.MessageType = MessageType.Private) then
//                                    _message.SendPrivate(msgInfo.PlayerId, errors, SoundType.None)
//                                else if (msgInfo.MessageType = MessageType.PrivateRemote) then
//                                    _message.SendPrivate(msgInfo.PlayerId, errors, SoundType.None)
//                    else
//                        ()
//                    ()
//                else
//                    if (msgInfo.MessageType = MessageType.Private) then
//                        _message.SendPrivate(msgInfo.PlayerId, "Unknown command. Try !help", SoundType.None)
//                    else if (msgInfo.MessageType = MessageType.PrivateRemote) then
//                        _message.SendPrivateRemote(msgInfo.PlayerName, "Unknown command. Try !help", SoundType.None)
//            else
//                if (msgInfo.MessageType = MessageType.Private) then
//                    _message.SendPrivate(msgInfo.PlayerId, "Unknown command. Try !help", SoundType.None)
//                else if (msgInfo.MessageType = MessageType.PrivateRemote) then
//                    _message.SendPrivateRemote(msgInfo.PlayerName, "Unknown command. Try !help", SoundType.None)
//
//
//    member private this.HelpCommandExecutedHandler sender (context:IFcCommandContext) =
//        context.ReplyPrivately(String.Format("{0,-20}-   {1}", "Module(s)", "Command(s)"))
//        for (mi : IFcEngineModuleInfo) in _info.ModuleInfoList do
//            let cmdNameStr = 
//                if (String.IsNullOrEmpty(mi.Alias)) then
//                    mi.Name
//                else
//                    mi.Name + "|" + mi.Alias
//
//            let mutable commands : IEnumerable<IFcCommand> = null
//            if (_coreManager.TryGetCommands(mi.Name, &commands)) then
//                let mutable cmdsStr = String.Empty
//                for cmd in commands do
//                    cmdsStr <- cmdsStr + cmd.Name + "|" + cmd.Alias + ","
//                cmdsStr <- cmdsStr.TrimEnd(',')
//                let miStr = String.Format("{0,20}:   {1}", cmdNameStr, cmdsStr)
//                context.ReplyPrivately(miStr)
//        context.ReplyPrivately("-")
//        context.ReplyPrivately("Usage: !moduleName|Alias.cmdName|Alias (example: !fccore.help !fc.h)")
//        //context.ReplyPrivately("Usage: !moduleName|Alias.cmdName|Alias ? (Query information about this command.)")
//        //context.ReplyPrivately("!help <module> (View detailed help for specified module's commands.)")
//        //context.ReplyPrivately("!help <module.command> (View detailed help for specified module's command.)")
//        ()
//
//    member private this.AboutCommandExecutedHandler sender (context:IFcCommandContext) =
//        context.ReplyPrivately(" - ModuleName|Alias - ")
//        for (mi : IFcEngineModuleInfo) in _info.ModuleInfoList do
//            let cmdNameStr = 
//                if (String.IsNullOrEmpty(mi.Alias)) then
//                    mi.Name
//                else
//                    mi.Name + "|" + mi.Alias
//            let miStr = String.Format("{0,20}: {1,-10} by {2,-19} ({3})", cmdNameStr, mi.Version, mi.Author, mi.Description)
//            context.ReplyPrivately(miStr)
//        ()

        //packet.
//        let filename = packet.ReadStringUntilNullTerminated(1, 16)
//        let file = File.Create(filename)
//        file.Write(packet.Buffer, 17, packet.Size - 1 - 16)
//        file.Close()
//        printf "File %s Downloaded. %d %d" filename packet.Size packet.Buffer.Length
//            let stuff : byte[] = Array.zeroCreate ((int)file.Length)
//            let readNumb = file.Read(stuff, 0, (int)file.Length)

//            //let unzipper = new ICSharpCode.SharpZipLib.Zip.Compression.Streams.InflaterInputStream(zippedData, new ICSharpCode.SharpZipLib.Zip.Compression.Inflater(false));
//            
//            let zippedFileStream = new MemoryStream()
//            let deflater = new ICSharpCode.SharpZipLib.Zip.Compression.Deflater(ICSharpCode.SharpZipLib.Zip.Compression.Deflater.DEFAULT_COMPRESSION, false)
//            let deflaterStream = new ICSharpCode.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream(zippedFileStream, deflater) :> Stream
//
//            deflaterStream.Write(stuff, 0, (int)file.Length)
//            deflaterStream.Close()
////
//            let output = zippedFileStream.ToArray()
            //let output = Array.zeroCreate 0