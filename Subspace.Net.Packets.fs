﻿// Copyright (c) 2012 Fc <fcx.code@gmail.com>
//            All Rights Reserved
//
// This software is provided 'as-is', without any express or implied 
// warranty. In no event will the author be held liable for any damages 
// arising from the use of this software.
//
namespace EGSoft.Fc.SubspaceModel.Net.Packets

open System
open LanguagePrimitives

open EGSoft.Fc.SubspaceModel
open EGSoft.Fc.SubspaceModel.Net.Utilities
open EGSoft.Fc.SubspaceModel.Net

[<AllowNullLiteral>]
type IPacket = 
    abstract Type : unit -> byte
        with get

[<AllowNullLiteral>]
type IPacketC2S = 
    inherit IPacket
    abstract Size : unit -> int
        with get
    abstract Serialize : byte[] * int -> int

[<AllowNullLiteral>]
type IPacketS2C =
    inherit IPacket

[<AllowNullLiteral>]
type IPacketCore =
    inherit IPacket
    inherit IPacketS2C
    inherit IPacketC2S
//    abstract CoreType : unit -> byte
//        with get

//[<AllowNullLiteral>]
//type IPacketFactory<'PacketType> when 'PacketType : delegate<byte[] * int * int, 'PacketType> =
//    abstract PacketS2CType : unit -> byte
//        with get
//    abstract Create : byte[] * int * int -> PacketType
//
[<AllowNullLiteral>]
type internal IPacketS2CReader =
    abstract Buffer : unit -> byte[]
        with get
    abstract Index : unit -> int
        with get
    abstract Size : unit -> int
        with get

type PacketS2CReader =
    struct
        val internal Buffer : byte[]
        val internal Index : int
        val Size : int
        new(buffer, index, size) =
            {
                Buffer = buffer;
                Index = index;
                Size = size;
            }
        member this.S2CType = this.Buffer.[this.Index]
        member this.ReadByte(index : int) = this.Buffer.[this.Index + index]
        member this.ReadUInt16(index : int) = PacketBuilder.ReadUInt16(this.Buffer, this.Index + index)
        member this.ReadInt16(index : int) = PacketBuilder.ReadInt16(this.Buffer, this.Index + index)
        member this.ReadUInt32(index : int) = PacketBuilder.ReadUInt32(this.Buffer, this.Index + index)
        member this.ReadInt32(index : int) = PacketBuilder.ReadInt32(this.Buffer, this.Index + index)
        member this.ReadByteArray(index : int, size : int) = PacketBuilder.ReadByteArray(this.Buffer, this.Index + index, size) 
        member this.ReadString(index : int, size : int) = PacketBuilder.ReadString(this.Buffer, this.Index + index, size)
        member this.ReadStringUntilNullTerminated(index : int, size : int) = PacketBuilder.ReadStringUntilNullTerminated(this.Buffer, this.Index + index, size)
    end

module Packet =
    let MaxSize = 512