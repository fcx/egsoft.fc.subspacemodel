﻿// Copyright (c) 2012 Fc <fcx.code@gmail.com>
//            All Rights Reserved
//
// This software is provided 'as-is', without any express or implied 
// warranty. In no event will the author be held liable for any damages 
// arising from the use of this software.
//
namespace EGSoft.Fc.SubspaceModel.Net.Packets.Core.Utilities

open System
open LanguagePrimitives
open System.Collections.Generic

open EGSoft.Fc.SubspaceModel
open EGSoft.Fc.SubspaceModel.Net
open EGSoft.Fc.SubspaceModel.Net.Utilities
open EGSoft.Fc.SubspaceModel.Net.Packets
open EGSoft.Fc.SubspaceModel.Net.Packets.Core

type BigChunkManager() =
    let mutable bigChunk : byte[] = null
    let mutable bigChunkLengthSoFar = 0

    member this.Reset() =
        bigChunk <- null
        bigChunkLengthSoFar <- 0

    member this.AddBody(buffer : byte[], index : int, size : int) =
        let totalLength = PacketBuilder.ReadInt32(buffer, index + 2);
        let dataLength = size - 6
        if (bigChunk = null || bigChunk.Length <> totalLength) then
            bigChunk <- Array.zeroCreate totalLength
            PacketBuilder.WriteByteArray(bigChunk, 0, buffer, index + 6, dataLength)
            bigChunkLengthSoFar <- dataLength
        else
            PacketBuilder.WriteByteArray(bigChunk, bigChunkLengthSoFar, buffer, index + 6, dataLength)
            bigChunkLengthSoFar <- bigChunkLengthSoFar + dataLength
        (bigChunkLengthSoFar = totalLength)

    member this.EmptyBuffer() : byte[]=
        let bigChunkBuffer = bigChunk
        bigChunk <- null
        bigChunkBuffer

    member this.BreakUp(packet : IPacketC2S) =
        let bigChunkTotalLength = packet.Size
        let bigChunkBuffer : byte[] = Array.zeroCreate (bigChunkTotalLength)
        packet.Serialize(bigChunkBuffer, 0) |> ignore

        let maxPacketSize = (Packet.MaxSize - Reliable.HeaderSize - 6 - 1)
        let totalPacketsCount = (int32)(Math.Ceiling((float)packet.Size / (float)(maxPacketSize)))

        [| for i in 0 .. totalPacketsCount - 1 -> 
            let index = i*maxPacketSize
            let size = if (i = totalPacketsCount - 1) then (bigChunkTotalLength - index) else maxPacketSize
            new BigChunkBody(bigChunkTotalLength, bigChunkBuffer, index, size)
        |]
            


type SmallChunkManager() =
    let smallChunkList = new List<PacketS2CReader>()
    let mutable smallChunkSize = 0

    member this.Reset() =
        smallChunkList.Clear()
        smallChunkSize <- 0

    member this.AddBody(buffer : byte[], index : int, size : int) =
        //let data = buffer.[index + 2 .. size - 1]
        let data = new PacketS2CReader(buffer, index + 2, size - 2)
        smallChunkSize <- smallChunkSize + data.Size
        smallChunkList.Add(data)

    member this.AddTail(buffer : byte[], index : int, size : int) : byte[] =
        smallChunkSize <- smallChunkSize + size
        let smallChunk : byte[] = Array.zeroCreate smallChunkSize
        let mutable index = 0
        for i in 0 .. smallChunkList.Count - 1 do
            let tempData = smallChunkList.[i]
            PacketBuilder.WriteByteArray(smallChunk, index, tempData.Buffer, tempData.Index, tempData.Size) |> ignore
            index <- index + tempData.Size
        PacketBuilder.WriteByteArray(smallChunk, index, buffer, 2, size) |> ignore
        smallChunkList.Clear()
        smallChunkSize <- 0
        smallChunk

    member this.BreakUp(packet : IPacketC2S) =
        let packetTotalLength = packet.Size
        let smallChunkBuffer : byte[] = Array.zeroCreate (packetTotalLength)
        packet.Serialize(smallChunkBuffer, 0) |> ignore

        let maxSmallChunkSize = (Packet.MaxSize - Reliable.HeaderSize - 2 - 1)
        let totalSmallChunkCount = (int32)(Math.Ceiling((float)packetTotalLength / (float)maxSmallChunkSize))

        let smallChunkBodies = 
            [| for i in 0 .. totalSmallChunkCount - 2 -> 
                new SmallChunkBody(smallChunkBuffer, i*maxSmallChunkSize, maxSmallChunkSize)
            |]

        let smallChunkTailIndex = (totalSmallChunkCount - 1) * maxSmallChunkSize
        let smallChunkTail = new SmallChunkTail(smallChunkBuffer, smallChunkTailIndex, packetTotalLength - smallChunkTailIndex)
        smallChunkBodies, smallChunkTail