﻿// Copyright (c) 2012 Fc <fcx.code@gmail.com>
//            All Rights Reserved
//
// This software is provided 'as-is', without any express or implied 
// warranty. In no event will the author be held liable for any damages 
// arising from the use of this software.
//
namespace EGSoft.Fc.SubspaceModel

open System
open System.Collections
open System.Collections.Generic
open System.Runtime.InteropServices 

type RegistrationFormGenderType =
    | Male = 0x00uy
    | Female = 0x01uy
    | Secret = 0x02uy

type LoginResponseType =
    | Success = 0x00uy// 0   - Move along.
    | RegistrationRequired = 0x01uy// 1   - Unknown player continue as new user?
    | InvalidPassword = 0x02uy// 2   - Invalid password for specified user.  The name you have chosen is probably in use by another player try picking a different name.
    | FullArena = 0x03uy// 3   - This arena is currently full = 0x00uy try again later.
    | LockedOut = 0x04uy// 4   - You have been locked out of SubSpace, for more information inquire on Web BBS.
    | NoPermission = 0x05uy// 5   - You do not have permission to play in this arena  see Web Site for more information.
    | SpectateOnly = 0x06uy// 6   - You only have permission to spectate in this arena.
    | TooManyPoints = 0x07uy// 7   - You have too many points to play in this arena please choose another arena.
    | SlowConnection = 0x08uy// 8   - Your connection appears to be too slow to play in this arena.
    | NoPermission2 = 0x09uy// 9   - You do not have permission to play in this arena see Web Site for more information.
    | ServerIsFull = 0x0Auy// 10  - The server is currently not accepting new connections.
    | InvalidName = 0x0Buy// 11  - Invalid user name entered = 0x00uy please pick a different name.
    | ObsceneName = 0x0Cuy// 12  - Possibly offensive user name entered = 0x00uy please pick a different name.
    | BillerDown = 0x0Duy// 13  - NOTICE: Server difficulties; this zone is currently not keeping track of scores.  Your original score will be available later.  However you are free to play in the zone until we resolve this problem.
    | ServerBusy = 0x0Euy// 14  - The server is currently busy processing other login requests = please try again in a few moments.
    | ExperiencedOnly = 0x0Fuy// 15  - This zone is restricted to experienced players only (ie. certain number of game-hours logged).
    | UsingDemoVersion = 0x10uy// 16  - You are currently using the demo version.  Your name and score will not be kept track of.
    | TooManyDemos = 0x11uy// 17  - This arena is currently has(sic) the maximum Demo players allowed = 0x00uy try again later.
    | ClosedToDemos = 0x12uy// 18  - This arena is closed to Demo players.
    | UnknownResponse = 0x13uy// ... - Unknown response, please go to Web site for more information and to obtain latest version of the program.
    | ModeratorAccessOnly = 0xFFuy// 255 - Moderator access required for this zone (MGB addition)

type ArenaLoginType = 
    | UseArenaName = 0xFFFDus
    | UseRandomPublic = 0xFFFFus

type CustomSpawn =
    | PointZero
    | PointOne
    | PointTwo
    | PointThree

type ShipType = 
    | Warbird = 0x00uy
    | Javelin = 0x01uy
    | Spider = 0x02uy
    | Leviathan = 0x03uy
    | Terrier = 0x04uy
    | Weasel = 0x05uy
    | Lancaster = 0x06uy
    | Shark = 0x07uy
    | Spectate = 0x08uy

type WeaponType =
    | None = 0x00uy
    | Bullet = 0x01uy
    | BounceBullet = 0x02uy
    | Bomb = 0x03uy
    | ProxyBomb = 0x04uy
    | Repel = 0x05uy
    | Decoy = 0x06uy
    | Burst = 0x07uy
    | Thor = 0x08uy

type WeaponLevel = 
    | One = 0x00uy
    | Two = 0x01uy
    | Three = 0x02uy
    | Four = 0x03uy

type ShrapnelLevel = 
    | One = 0x00uy
    | Two = 0x01uy
    | Three = 0x02uy
    | Four = 0x03uy

type SoundType =
    | None = 0x00uy             // 0  = Silence
    | BassBeep = 1uy            // 1  = BEEP!
    | TrebleBeep = 2uy          // 2  = BEEP!
    | ATT = 3uy                 // 3  = You're not dealing with AT&T
    | Discretion = 4uy          // 4  = Due to some violent content = 0x00uy parental discretion is advised
    | Hallellula = 5uy          // 5  = Hallellula
    | Reagan = 6uy              // 6  = Ronald Reagan
    | Inconceivable = 7uy       // 7  = Inconceivable
    | Churchill = 8uy           // 8  = Winston Churchill
    | SnotLicker = 9uy          // 9  = Listen to me = 0x00uy you pebble farting snot licker
    | Crying = 10uy             // 10 = Crying
    | Burp = 11uy               // 11 = Burp
    | Girl = 12uy               // 12 = Girl
    | Scream = 13uy             // 13 = Scream
    | Fart = 14uy               // 14 = Fart1
    | Fart2 = 15uy              // 15 = Fart2
    | Phone = 16uy              // 16 = Phone ring
    | WorldUnderAttack = 17uy   // 17 = The world is under attack at this very moment
    | Gibberish = 18uy          // 18 = Gibberish
    | Ooooo = 19uy              // 19 = Ooooo
    | Geeee = 20uy              // 20 = Geeee
    | Ohhhh = 21uy              // 21 = Ohhhh
    | Ahhhh = 22uy              // 22 = Awwww
    | ThisGameSucks = 23uy      // 23 = This game sucks
    | Sheep = 24uy              // 24 = Sheep
    | CantLogIn = 25uy          // 25 = I can't log in!
    | MessageAlarm = 26uy       // 26 = Beep
    | StartMusic = 100uy        // 100= Start music playing
    | StopMusic = 101uy         // 101= Stop music
    | PlayOnce = 102uy          // 102= Play music for 1 iteration then stop
    | VictoryBell = 103uy       // 103= Victory bell
    | Goal = 104uy              // 104= Goal!

type AudioType =
    | None = 0x0000us
    | On = 0x00001us

type MessageType =
    | Broadcast = 0x00uy
    | PublicMacro = 0x01uy
    | Public = 0x02uy
    | Team = 0x03uy
    | TeamBroadcast = 0x04uy
    | Private = 0x05uy
    | Server = 0x06uy
    | PrivateRemote = 0x07uy
    | Warning = 0x08uy
    | Chat = 0x09uy

//type MessageInfo =
//    {   ChatType : ChatType
//        SoundType : SoundType
//        SenderPlayerId : uint16;
//        SenderPlayerName : String;
//        Message : String
//    }

type MessageInfo =
    struct
        val MessageType : MessageType
        val SoundType : SoundType
        val private PlayerOrChatId : uint16
        val PlayerName : String
        val Message : String
        new (messageType, soundType, playerOrChatId, playerName, message) =
            {
                MessageType = messageType
                SoundType = soundType
                PlayerOrChatId = playerOrChatId
                PlayerName = playerName
                Message = message
            }
        member this.PlayerId = this.PlayerOrChatId
        member this.ChatId = this.PlayerOrChatId
    end

type ArenaInfo =
    struct
        val Name : String;
        val Population : int;
        new (name, population) =
            {   
                Name = name;
                Population = population; 
            }
    end

type ArenaInfoList =
    class
        val private arenaInfoList : List<ArenaInfo>;
        val mutable inArena : String
        val mutable totalPopulation : int
        new (arenaInfoArray : ArenaInfo list) as this =
            { 
                arenaInfoList = new List<ArenaInfo>()
                totalPopulation = 0
                inArena = String.Empty
            }
            then
                for i in 0 .. arenaInfoArray.Length - 1 do
                    let ai = arenaInfoArray.[i]
                    if ai.Population > 0 then
                        this.totalPopulation <- this.totalPopulation + ai.Population
                        this.arenaInfoList.Add(ai) 
                    else
                        let newAI = ArenaInfo(ai.Name, ai.Population * -1)
                        this.totalPopulation <- this.totalPopulation + newAI.Population
                        this.arenaInfoList.Add(newAI) 

        member this.TotalPopulation = this.totalPopulation

        interface IEnumerable<ArenaInfo> with
            member this.GetEnumerator () =
                ((this.arenaInfoList.GetEnumerator()) :> IEnumerator<ArenaInfo>)
        interface IEnumerable with
            member this.GetEnumerator () =
                (this.arenaInfoList.GetEnumerator()) :> IEnumerator
    end
    
[<AllowNullLiteral>]
type PlayerInfo =
    class    
        val mutable private shipType : ShipType;
        val private acceptAudioMessage : bool;
        val private name : String;
        val private squad : String;
        val mutable private flagPoints : uint32;
        val mutable private killPoints : uint32;
        val private playerId : uint16;
        val mutable private frequency : uint16;
        val mutable private kills : uint16;
        val mutable private deaths : uint16;
        val private turreteePlayerId : uint16;
        val private flagCount : uint16;
        val private hasKOTH : bool;
        new (_shipType, _acceptAudioMessage, _name, _squad, _flagPoints, _killPoints, _playerId, _frequency, _kills, _deaths, _turreteePlayerId, _flagCount, _hasKOTH) =
            {
                shipType = _shipType
                acceptAudioMessage = _acceptAudioMessage
                name = _name
                squad = _squad
                flagPoints = _flagPoints
                killPoints = _killPoints
                playerId = _playerId
                frequency = _frequency
                kills = _kills
                deaths = _deaths
                turreteePlayerId = _turreteePlayerId
                flagCount = _flagCount
                hasKOTH = _hasKOTH
            }


        member this.PlayerId = this.playerId
        member this.Name = this.name
        member this.Squad = this.squad
        member this.Frequency
            with get() = this.frequency
            and internal set v = this.frequency <- v
        member this.ShipType
            with get() = this.shipType
            and internal set v = this.shipType <- v
        member this.Deaths
            with get() = this.deaths
            and internal set v = this.deaths <- v
        member this.Kills
            with get() = this.kills
            and internal set v = this.kills <- v
        member this.KillPoints
            with get() = this.killPoints
            and internal set v = this.killPoints <- v
        member this.FlagPoints
            with get() = this.flagPoints
            and internal set v = this.flagPoints <- v
    
    end


type PlayerInfoMap =
    class
        val playerIdToPlayerInfoMap : Dictionary<uint16,PlayerInfo>
        val playerNameToPlayerInfoMap : Dictionary<string,PlayerInfo>
        new () =
            {
                playerIdToPlayerInfoMap = new Dictionary<uint16, PlayerInfo>()
                playerNameToPlayerInfoMap = new Dictionary<string, PlayerInfo>(StringComparer.OrdinalIgnoreCase)
            }
        member this.Clear() =
            this.playerIdToPlayerInfoMap.Clear()
            this.playerNameToPlayerInfoMap.Clear()
        
//        member this.TryGetPlayer(playerId : uint16) = this.playerIdToPlayerInfoMap.TryGetValue playerId
//        member this.TryGetPlayer(name : String) = this.playerNameToPlayerInfoMap.TryGetValue name

        //member this.TryGetPlayera = this.playerIdToPlayerInfoMap.TryGetValue
        member this.TryGetPlayer(playerId : uint16, [<Out>] value : byref<PlayerInfo>) = this.playerIdToPlayerInfoMap.TryGetValue(playerId,&value)
        member this.TryGetPlayer(name : String, [<Out>] value : byref<PlayerInfo>) = this.playerNameToPlayerInfoMap.TryGetValue(name,&value)

        member this.Add(playerInfo : PlayerInfo) =
            this.playerIdToPlayerInfoMap.Add(playerInfo.PlayerId, playerInfo)
            this.playerNameToPlayerInfoMap.Add(playerInfo.Name, playerInfo)

        member this.Remove(playerInfo : PlayerInfo) =
            this.playerIdToPlayerInfoMap.Remove(playerInfo.PlayerId) |> ignore
            this.playerNameToPlayerInfoMap.Remove(playerInfo.Name) |> ignore
    end

type TeamInfoMap =
    class
        val freqToPlayerInfoMap : Dictionary<uint16,List<PlayerInfo>>
        new () =
            {
                freqToPlayerInfoMap = new Dictionary<uint16, List<PlayerInfo>>()
            }
        member this.Clear() =
            this.freqToPlayerInfoMap.Clear()
        
        //member this.TryGetTeam(playerId : uint16) = this.freqToPlayerInfoMap.TryGetValue playerId
        member this.TryGetTeam(playerId : uint16, [<Out>] value : byref<List<PlayerInfo>>) = this.freqToPlayerInfoMap.TryGetValue(playerId,&value)

        member this.Add(playerInfo : PlayerInfo) =
            let freq = playerInfo.Frequency
            let mutable team = null
            if (not (this.freqToPlayerInfoMap.TryGetValue(freq, &team))) then
                team <- new List<PlayerInfo>()
                this.freqToPlayerInfoMap.Add(freq, team)
            team.Add(playerInfo)
            
        member this.Remove(playerInfo : PlayerInfo) =
            let freq = playerInfo.Frequency
            let team = this.freqToPlayerInfoMap.[freq]
            team.Remove(playerInfo) |> ignore
            if (team.Count = 0) then
                this.freqToPlayerInfoMap.Remove(freq) |> ignore

        interface IEnumerable<KeyValuePair<uint16,List<PlayerInfo>>> with
            member this.GetEnumerator () =
                (this.freqToPlayerInfoMap.GetEnumerator()) :> IEnumerator<KeyValuePair<uint16,List<PlayerInfo>>>
        interface IEnumerable with
            member this.GetEnumerator () =
                (this.freqToPlayerInfoMap.GetEnumerator()) :> IEnumerator
    end