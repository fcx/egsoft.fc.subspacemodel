﻿// Copyright (c) 2012 Fc <fcx.code@gmail.com>
//            All Rights Reserved
//
// This software is provided 'as-is', without any express or implied 
// warranty. In no event will the author be held liable for any damages 
// arising from the use of this software.
//
namespace EGSoft.Fc.SubspaceModel.Net.Packets.C2S

open System
open System.Collections.Generic
open LanguagePrimitives

open EGSoft.Fc.SubspaceModel
open EGSoft.Fc.SubspaceModel.Net
open EGSoft.Fc.SubspaceModel.Net.Utilities
open EGSoft.Fc.SubspaceModel.Net.Packets

type LoginRequest =
    struct
        val Name : String;
        val Password : String;
        val NewUser : bool;

        new (name, password, newUser) =
            {   Name = name;
                Password = password;
                NewUser = newUser;
            }

        interface IPacketC2S with
            member x.get_Type() = EnumToValue PacketC2SType.LoginRequest
            member this.get_Size() = 101
            member this.Serialize(buffer : byte[] , index : int) = 
                //buffer.[index + 0] <- EnumToValue PacketC2SType.LoginRequest
                buffer.[index + 1] <- if this.NewUser then 0x01uy else 0x00uy             //NewUser?

                PacketBuilder.WriteString(buffer, index + 2, this.Name, 32)         //Name
                PacketBuilder.WriteString(buffer, index + 34, this.Password, 32)
                
                PacketBuilder.WriteUInt32(buffer, index + 66, 0x64eb67d8u) // Machine ID - 4 bytes //

                buffer.[index + 70] <- 0uy
                PacketBuilder.WriteUInt16(buffer, index + 71, 0x0000us)                // Timezone bias (224 240=EST) - 2 bytes

                PacketBuilder.WriteUInt16(buffer, index + 73, 0x9d6fus)                // Unknown - 2bytes

                PacketBuilder.WriteUInt16(buffer, index + 75, 0x86us)          // Client version (SS 1.34)
                PacketBuilder.WriteInt32(buffer, index + 77, 444)             // Memory checksum
                PacketBuilder.WriteInt32(buffer, index + 81, 555)             // Memory checksum
                PacketBuilder.WriteUInt32(buffer, index + 85, 0x92f88614u)      // Permission ID
                (this :> IPacketC2S).Size
    end

    
type RegistrationForm =
    struct
        val Name : String;
        val Email : String;
        val City : String;
        val State : String;
        val Sex : RegistrationFormGenderType;
        val Age : byte;

        new (name, email, city, state, sex, age) =
            {   Name = name;
                Email = email;
                City = city;
                State = state;
                Sex = sex;
                Age = age; 
            }
        interface IPacketC2S with
            member x.get_Type() = EnumToValue PacketC2SType.RegistrationForm
            member this.get_Size() = 766
            member this.Serialize(buffer : byte[] , index : int) = 
                //buffer.[index + 0] <- EnumToValue PacketC2SType.RegistrationForm
                PacketBuilder.WriteString(buffer, index + 1, this.Name, 32)  
                PacketBuilder.WriteString(buffer, index + 33, this.Email, 64)    
                PacketBuilder.WriteString(buffer, index + 97, this.City, 32)    
                PacketBuilder.WriteString(buffer, index + 129, this.State, 24) 
                
                buffer.[index + 153] <- 0x00uy
                buffer.[index + 154] <- this.Age           

                buffer.[index + 155] <- 0x01uy   
                buffer.[index + 156] <- 0x01uy   
                buffer.[index + 157] <- 0x01uy                

                PacketBuilder.WriteInt32(buffer, index + 158, 586)             
                PacketBuilder.WriteUInt16(buffer, index + 162, 0xC000us)
                PacketBuilder.WriteUInt16(buffer, index + 164, 2036us) 

                PacketBuilder.WriteString(buffer, index + 166, this.Name, 40)
                PacketBuilder.WriteString(buffer, index + 206, "FcBot", 40) 
                PacketBuilder.WriteString(buffer, index + 246, "FcBot", 40) 
                PacketBuilder.WriteString(buffer, index + 286, "FcBot", 40) 
                PacketBuilder.WriteString(buffer, index + 326, "FcBot", 40) 
                PacketBuilder.WriteString(buffer, index + 366, "FcBot", 40) 
                PacketBuilder.WriteString(buffer, index + 406, "FcBot", 40) 
                PacketBuilder.WriteString(buffer, index + 446, "FcBot", 40) 
                PacketBuilder.WriteString(buffer, index + 486, "FcBot", 40) 
                PacketBuilder.WriteString(buffer, index + 526, "FcBot", 40) 
                PacketBuilder.WriteString(buffer, index + 566, "FcBot", 40) 
                PacketBuilder.WriteString(buffer, index + 606, "FcBot", 40)
                PacketBuilder.WriteString(buffer, index + 646, "FcBot", 40) 
                PacketBuilder.WriteString(buffer, index + 686, "FcBot", 40) 
                PacketBuilder.WriteString(buffer, index + 726, "FcBot", 40)
                (this :> IPacketC2S).Size
    end

type ArenaEnter =
    struct
        val ShipType : ShipType;
        val AudioType : AudioType;
        val XResolution : uint16;
        val YResolution : uint16;
        val ArenaNumber : uint16;
        val ArenaName : String;
        new(shipType, audioType, xResolution, yResolution, arenaNumber, arenaName) =
            {   ShipType = shipType
                AudioType = audioType
                XResolution = xResolution
                YResolution = yResolution
                ArenaNumber = arenaNumber
                ArenaName = arenaName
            }
        interface IPacketC2S with
            member x.get_Type() = EnumToValue PacketC2SType.ArenaEnter
            member this.get_Size() = 26
            member this.Serialize(buffer : byte[] , index : int) = 
                //PacketBuilder.Write(buffer, index + 0, EnumToValue PacketC2SType.ArenaEnter) |> ignore
                buffer.[index + 1] <- EnumToValue this.ShipType
                PacketBuilder.WriteUInt16(buffer, index + 2, EnumToValue this.AudioType) |> ignore
                PacketBuilder.WriteUInt16(buffer, index + 4, this.XResolution) |> ignore
                PacketBuilder.WriteUInt16(buffer, index + 6, this.YResolution) |> ignore
                PacketBuilder.WriteUInt16(buffer, index + 8, this.ArenaNumber) |> ignore
                PacketBuilder.WriteString(buffer, index + 10, this.ArenaName, 16) |> ignore
                (this :> IPacketC2S).Size
    end

type Position =
    struct
        val Direction : byte
        val Timestamp : uint32
        val XPoint : uint16
        val YPoint : uint16
        val XVelocity : int16
        val YVelocity : int16
        val Energy : uint16
        val Bounty : uint16
        val ShipInfo : byte
        val WeaponInfo : uint16
        new (direction, timestamp, xPoint, yPoint, xVelocity, yVelocity, energy, bounty, shipInfo, weaponInfo) =
            {   Direction = direction
                Timestamp  = timestamp
                XPoint = xPoint
                YPoint = yPoint
                XVelocity = xVelocity
                YVelocity  = yVelocity
                Energy = energy
                Bounty  = bounty
                ShipInfo = shipInfo
                WeaponInfo  = weaponInfo
            }
        interface IPacketC2S with
            member x.get_Type() = EnumToValue PacketC2SType.Position
            member this.get_Size() = 22
            member this.Serialize(buffer : byte[] , index : int) = 
                let mutable checksum = 0uy
                buffer.[index + 0] <- EnumToValue PacketC2SType.Position
                buffer.[index + 1] <- this.Direction
                PacketBuilder.WriteUInt32(buffer, index + 2, this.Timestamp)
                PacketBuilder.WriteInt16(buffer, index + 6, this.XVelocity)
                PacketBuilder.WriteUInt16(buffer, index + 8, this.YPoint) 
                buffer.[index + 10] <- checksum
                buffer.[index + 11] <- this.ShipInfo
                PacketBuilder.WriteUInt16(buffer, index + 12, this.XPoint) 
                PacketBuilder.WriteInt16(buffer, index + 14, this.YVelocity)
                PacketBuilder.WriteUInt16(buffer, index + 16, this.Bounty) 
                PacketBuilder.WriteUInt16(buffer, index + 18, this.Energy) 
                PacketBuilder.WriteUInt16(buffer, index + 20, this.WeaponInfo) 
                for i in 0 .. 21 do
                    checksum <- checksum ^^^ buffer.[i + index]
                buffer.[index + 10] <- checksum
                (this :> IPacketC2S).Size
    end

type KotHEndTimer =
    struct
        interface IPacketC2S with
            member x.get_Type() = EnumToValue PacketC2SType.KotHEndTimer
            member this.get_Size() = 1
            member this.Serialize(buffer : byte[] , index : int) = 
                let mutable checksum = 0uy
                //PacketBuilder.Write(buffer, index + 0, EnumToValue PacketC2SType.KotHEndTimer) |> ignore
                (this :> IPacketC2S).Size
    end

type MessageOut =
    struct
        val MessageType : MessageType;
        val SoundType : SoundType;
        val PlayerId : uint16;
        val Message : String;
        new (messageType, soundType, playerId, message) =
            {   MessageType = messageType
                SoundType = soundType;
                PlayerId = playerId;
                Message = message;
            }
        static member MaxSize = 256 - 6 - 1 //server only allows max of that
        interface IPacketC2S with
            member x.get_Type() = EnumToValue PacketC2SType.Chat
            member this.get_Size() = this.Message.Length + 6
            member this.Serialize(buffer : byte[] , index : int) = 
                buffer.[index + 0] <- EnumToValue PacketC2SType.Chat
                buffer.[index + 1] <- EnumToValue this.MessageType
                buffer.[index + 2] <- EnumToValue this.SoundType
                PacketBuilder.WriteUInt16(buffer, index + 3, this.PlayerId) |> ignore
                PacketBuilder.WriteString(buffer, index + 5, this.Message, MessageOut.MaxSize) |> ignore
                (this :> IPacketC2S).Size
    end

type ArenaLeave =
    struct        
        interface IPacketC2S with
            member x.get_Type() = EnumToValue PacketC2SType.ArenaLeave
            member this.get_Size() = 1
            member this.Serialize(buffer : byte[] , index : int) = 
                buffer.[index + 0] <- EnumToValue PacketC2SType.ArenaLeave
                (this :> IPacketC2S).Size
    end


type FileTransferOut =
    struct
        val Filename : String;
        val FileData : byte[]
        new (filename, fileData) =
            {   Filename = filename
                FileData = fileData;
            }
        interface IPacketC2S with
            member x.get_Type() = EnumToValue PacketC2SType.FileTransfer
            member this.get_Size() = this.FileData.Length + 16 + 1
            member this.Serialize(buffer : byte[] , index : int) = 
                //buffer.[index + 0] <- EnumToValue PacketC2SType.Core
                //buffer.[index + 1] <- EnumToValue PacketCoreType.BigChunkBody
                //PacketBuilder.WriteInt32(buffer, index + 2, 17 + 20)

                buffer.[index + 0] <- EnumToValue PacketC2SType.FileTransfer
                PacketBuilder.WriteString(buffer, index + 1, this.Filename, 16) |> ignore
                PacketBuilder.WriteByteArray(buffer, index + 17, this.FileData, 0, this.FileData.Length) |> ignore
                (this :> IPacketC2S).Size
    end